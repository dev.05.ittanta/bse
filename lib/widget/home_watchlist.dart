import 'package:flutter/material.dart';
import 'package:flutter_bse/views/watchlist.dart';


class HomeWatchList extends StatefulWidget {
  final String title;
  final String price;
  final String percentage;

  final Function tap;
  final bool isHome;

  HomeWatchList({
    Key key,
    @required this.title,
    @required this.price,
    @required this.percentage,
    this.tap, this.isHome})
      : super(key: key);

  @override
  _HomeWatchListState createState() => _HomeWatchListState();
}

class _HomeWatchListState extends State<HomeWatchList> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.isHome?(){
        // Navigator.of(context).push(
        //   MaterialPageRoute(
        //     builder: (BuildContext context){
        //       return WatchList();
        //     },
        //   ),
        // );
      }:widget.tap,
      child: Card(
        shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(10.0)),
        elevation: 4.0,
        child: Padding(
          padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
          child:  Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(height: 10.0),
              Text(
                "${widget.title}",
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Arial'
                ),
              ),

              Row(
                children: [
                  Text(
                    "${widget.price}",
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Arial'
                    ),
                  ),
                  SizedBox(width: 10.0),
                  Text(
                    "${widget.percentage}",
                    style: TextStyle(
                      color: Colors.red,
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Arial'
                    ),
                  ),
                  SizedBox(width: 10.0),
                  Text(
                    "${widget.percentage}",
                    style: TextStyle(
                        color: Colors.red,
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Arial'
                    ),
                  ),
                ],
              ),
              SizedBox(height: 5),
            ],
          ),
        ),
      ),
    );
  }
}
