import 'package:flutter/material.dart';
import 'package:flutter_bse/utils/custom_color.dart';

class Constants {
  static String APP_NAME = "BseNew";
  static String APP_TOKEN = "token";
  static var INTERNET_CHECK = "Your Internet is Connected";
  static var GAINER = "Gainer";
  static var LOSERS = "Losers";
  static var TOP_TURNOVER = "Top Turnover";
  static var FIFTYTWO_WK_HIGH = "52 WK High";
  static var FIFTYTWO_WK_LOW = "52 WK Low";



  //BASE URL
  static String BASE_URL = "https://api.bseindia.com/";

  //Home Header
  static String HEADER_URL = "bseindia/api/Sensex/getSensexData";

  //Home Indices
  static String INDICES_URL = "bseindia/api/Indexmasternew/GetData";

  //Home Commodity
  static String COMMODITY_URL = "BseIndiaAPI/api/CommHeatMap/m?";

  //Home Currency
  static String CURRENCY_URL = "BseIndiaAPI/api/CurrHeatMap/m?";

  //Home Sensex
  static String SENSEX_URL = "bseindia/api/IndexConstituentsnew/GetData";
  static String SENSEX_URL_2 = "bseindia/api/GetQuotescripnew/GetData";

  //Equity
  static String EQUITY_GAINER_URL = "BseIndiaAPI/api/NewGainersAndroid/w?ln=en";
  static String EQUITY_LOSAR_URL = "BseIndiaAPI/api/NewLosersAndroid/w?";
  static String EQUITY_TOURN_OVER_URL = "BseIndiaAPI/api/NewTurnoverAndroid/w?";
  static String EQUITY_5WK_HIGH_URL = "BseIndiaAPI/api/NewHighLow52Week/w?";
  static String EQUITY_5WK_Low_URL = "BseIndiaAPI/api/NewHighLow52Week/w?";

  //Encrypt Key
  static String ENCRYPT_KEY_IV = "BseIndiaApi@2020";



}
