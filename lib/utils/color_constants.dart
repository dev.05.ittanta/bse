import 'package:flutter/material.dart';

class ColorConstants {
  static Color colorPrimary = Color(0xFF121212);
  static Color colorPrimaryDarkTheme = Color(0xFF121212);
  static Color colorPrimaryLightTheme = Color(0xfffcfcff);

  static MaterialColor whitecolor = new MaterialColor(
    0xFFFFFFFF,
    <int, Color>{
      50: Color(0xFFA4A4BF),
      100: Color(0xFFA4A4BF),
      200: Color(0xFFA4A4BF),
      300: Color(0xFF9191B3),
      400: Color(0xFF7F7FA6),
      500: Color(0xFF181861),
      600: Color(0xFF6D6D99),
      700: Color(0xFF5B5B8D),
      800: Color(0xFF494980),
      900: Color(0xFF181861),
    },
  );
  static Color colorPrimaryDark = Color(0xFF003d9a);
  static Color colorPrimaryLight = Color(0xFF5c93ff);
  static Color colorAccent = Color(0xFFFF5722);
  static Color colorAccentDarkTheme = Color(0xFFFFAB91);
  static Color orderListCard = Color(0xffe3f2fd);


  static Color blue = Color(0xFFFF00FA);
  static Color red = Color(0xFFe15241);
  static Color reqallocation = Color(0xFF398579);
  static Color pendingtickets = Color(0xFF97c15b);
  static Color pendingapprovals = Color(0xFFf4c343);
  static Color pendingaudits = Color(0xFF55b9d1);

  static Color button = HexColor("FF7BFB");
  static Color signInButton = HexColor("FF00FA");
  static Color signUpButton = Color(0xFF696969);
  static Color white = HexColor("FFFFFF");
  static Color black = HexColor("000000");
  static Color lightBlack = Color(0xFF191919);
  static Color backgroundColor = Color(0xFF121212);
  static Color darkAppBarColor = Color(0xFF272727);
  //static Color lightBlack = Color(0xFF262626);
  static Color green = Color(0xFF60A7A1);
  static Color statusbar = Color(0xFF373435);
  static Color grey = Color(0xFFDCDCDC);
  static Color background = Color(0xFFABEEF8);
  static Color topbar = Color(0xFF142B4B);
  static Color lightblack = Color(0xFF4C4C4C);
  static Color login_background = Color(0xFF11CDF2);
  static Color greydark = Color(0xFF939393);
  static Color table_header = Color(0xFFABEDF9);
  static Color splash = HexColor("ADD8E6");
  static Color dotcolor = HexColor("ea992f");
  static Color android_blue_dark = Color(0xFFED3237);
  static Color android_green_dark = Color(0xFF669900);
  static Color android_red_dark = Color(0xFFCC0000);
  static Color colorLeaveApprove = Color(0xFF669900);
  static Color colorLeaveDraft = Color(0xFFF7C81D);
  static Color colorLeaveRefuse = Color(0xFFff4444);
  static Color colorLeaveToApprove = Color(0xFFF7941D);
  static Color themeColor = Color(0xfff5a623);
  static Color primaryColor = Color(0xff203152);
  static Color greyColor = Color(0xffaeaeae);
  static Color greyColor2 = Color(0xffE8E8E8);
  static Color submitTourPlan = Color(0xFF0000ff);
  static Color approvedTourPlan = Color(0xFFff0000);
  static Color botttomsheet = Color(0xFF737373);
  static Color lightPrimary = Color(0xfffcfcff);
  static Color darkPrimary = Colors.black;
  static Color lightBG = HexColor("f1f2f6");
  static Color darkBG = Colors.black;
  static Color light_sky_blue = HexColor("72A3BF");
  static Color select_tab_color = HexColor("0a85c7");
  static Color light_tab_text_color = HexColor("a0a8ad");
  static Color grey_color = HexColor("AAAFBA");
  static Color light_grey_color = HexColor("d3d7da");
  static Color bg_color = HexColor("1A1F1F");
  static Color parrot_color = HexColor("19CF3E");
  static Color carrot_color = HexColor("F54845");
  static Color dark_skyblue_color = HexColor("000000");
  static Color dark_dot_inactive_color = HexColor("2b414f");
  static Color dark_gray = HexColor("121212");

  static Color lightAccent = Colors.red;
  static Color darkAccent = Colors.red[400];
}

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}
