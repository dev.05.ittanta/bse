import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter_bse/utils/color_constants.dart';

class Styles {

  static ThemeData themeData(bool isDarkTheme, BuildContext context) {

    return ThemeData(
      primarySwatch:  isDarkTheme ? Colors.indigo : Colors.indigo,
      primaryColor: isDarkTheme ? Colors.black : ColorConstants.colorPrimary,

      backgroundColor: isDarkTheme ? Color(0xFF151515) : Color(0xffF1F5FB),

      indicatorColor: isDarkTheme ? Color(0xff0E1D36) : Color(0xffCBDCF8),

      buttonColor: isDarkTheme ? Color(0xff3B3B3B) : ColorConstants.colorPrimary,

      hintColor: isDarkTheme ? Colors.white : Colors.black,
      // hintColor: isDarkTheme ? Color(0xff280C0B) : Color(0xffEECED3),

      //highlightColor: isDarkTheme ? Color(0xff372901) : Color(0xffFCE192),

      hoverColor: isDarkTheme ? Color(0xff3A3A3B) : Color(0xff4285F4),

      //focusColor: isDarkTheme ? Colors.white : Colors.black,
      focusColor: isDarkTheme ? Color(0xff0B2512) : Color(0xffA8DAB5),

      toggleableActiveColor: isDarkTheme ? ColorConstants.colorAccentDarkTheme : ColorConstants.colorAccent,

      disabledColor: Colors.grey,
      textSelectionTheme: isDarkTheme ? TextSelectionThemeData(
        cursorColor: Colors.white,
        selectionColor: Colors.white,
        selectionHandleColor: Colors.white,) :
      TextSelectionThemeData(
        cursorColor: Colors.black,
        selectionColor: Colors.black,
        selectionHandleColor: Colors.black,
      ),
      //textSelectionColor: isDarkTheme ? Colors.white : Colors.black,
      cardColor: isDarkTheme ? Color(0xFF151515) : Colors.white,

      canvasColor: isDarkTheme ? ColorConstants.black : Colors.grey[50],

      brightness: isDarkTheme ? Brightness.dark : Brightness.light,

      buttonTheme: Theme.of(context).buttonTheme.copyWith(
          colorScheme: isDarkTheme ? ColorScheme.dark() : ColorScheme.light()),

      appBarTheme: isDarkTheme ? AppBarTheme(
        backgroundColor: ColorConstants.white,
        titleTextStyle: TextStyle(
            fontFamily: "Arial", color: ColorConstants.white),
        elevation: 4.0,
      ) : AppBarTheme(
        backgroundColor: ColorConstants.colorPrimary,
        titleTextStyle: TextStyle(
            fontFamily: "Arial", color: ColorConstants.white),
        elevation: 4.0,
      ) ,

      inputDecorationTheme: InputDecorationTheme(
        hintStyle: isDarkTheme ? TextStyle(color: Colors.white) : TextStyle(color: Colors.grey[800]) ,
        border: OutlineInputBorder(
            borderSide: isDarkTheme ? BorderSide(color: Colors.white) : BorderSide(color: Colors.grey[800])
        ),
        focusedBorder: OutlineInputBorder(
            borderSide: isDarkTheme ? BorderSide(color: Colors.white) : BorderSide(color: Colors.grey[800])
        ),
        labelStyle: TextStyle(
            color: isDarkTheme ? Colors.white : Colors.black,
        ),
      ),

      bottomNavigationBarTheme: BottomNavigationBarThemeData(
        backgroundColor: isDarkTheme ? ColorConstants.darkAppBarColor : Colors.white,
        selectedItemColor: isDarkTheme ? ColorConstants.colorPrimaryDarkTheme : ColorConstants.colorPrimary,
        unselectedItemColor: isDarkTheme ? Colors.white : Colors.grey,
        elevation: 12.0
      ),

      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
          foregroundColor: isDarkTheme ? MaterialStateProperty.all<Color>(Colors.black) : MaterialStateProperty.all<Color>(ColorConstants.white),
          backgroundColor: isDarkTheme ? MaterialStateProperty.all<Color>(ColorConstants.colorPrimaryDarkTheme) : MaterialStateProperty.all<Color>(ColorConstants.colorPrimary),
        ),
      ),

      outlinedButtonTheme: OutlinedButtonThemeData(
        style: ButtonStyle(
          side: isDarkTheme ? MaterialStateProperty.all(BorderSide(color: Colors.white)) : MaterialStateProperty.all(BorderSide(color: ColorConstants.colorPrimary)),
          foregroundColor: isDarkTheme ? MaterialStateProperty.all<Color>(Colors.white) : MaterialStateProperty.all<Color>(ColorConstants.colorPrimary),
          //backgroundColor: isDarkTheme ? MaterialStateProperty.all<Color>(ColorConstants.colorPrimaryDarkTheme) : MaterialStateProperty.all<Color>(ColorConstants.colorPrimary),
        ),
      ),

      textButtonTheme: TextButtonThemeData(
        style: ButtonStyle(
          foregroundColor: isDarkTheme ? MaterialStateProperty.all<Color>(Colors.white) : MaterialStateProperty.all<Color>(ColorConstants.colorPrimary),
          //backgroundColor: isDarkTheme ? MaterialStateProperty.all<Color>(ColorConstants.colorPrimaryDarkTheme) : MaterialStateProperty.all<Color>(ColorConstants.colorPrimary),
        ),
      ),

      floatingActionButtonTheme: FloatingActionButtonThemeData(
        foregroundColor: isDarkTheme ? Colors.black : ColorConstants.white,
        backgroundColor: isDarkTheme ? ColorConstants.colorAccentDarkTheme : ColorConstants.colorAccent,
      ),

      iconTheme: IconThemeData(
        color: isDarkTheme ? ColorConstants.colorPrimaryDarkTheme : ColorConstants.colorPrimary,
      ),

      cardTheme: CardTheme(
        elevation: 4.0,
        color: isDarkTheme ? ColorConstants.backgroundColor : ColorConstants.white,
      ),

    );
  }
}