import 'package:flutter_bse/model/commodity_model.dart';
import 'package:flutter_bse/model/currency_model.dart';
import 'package:flutter_bse/model/equity_gainer_model.dart';
import 'package:flutter_bse/model/home_indices.dart';
import 'package:flutter_bse/model/sensex_model.dart';
import 'package:flutter_bse/model/sensex_scode_model.dart';
import 'package:flutter_bse/resources/app_api_provider.dart';

class Repository {
  final apiProvider = AppApiProvider();

  Future<List<dynamic>> getHomeHeaderDetails(String jsonEncode) =>
      apiProvider.getHomeHeaderData(jsonEncode);

  Future<List<HomeIndices>> getHomeIndicesDetails() =>
      apiProvider.getHomeIndicesData();

  Future<List<CommodityModel>> getCommodityDetails() =>
      apiProvider.getCommodityData();

  Future<List<CurrencyModel>> getCurrencyDetails() =>
      apiProvider.getCurrencyData();

  Future<List<SensexModel>> getSensex() => apiProvider.getSensexData();

  Future<ScodeModel> getScode(String scode) => apiProvider.getScodeData(scode);

  Future<GainerModel> getGainerDetails(String encryptKey) =>
      apiProvider.getGainerData(encryptKey);

}
