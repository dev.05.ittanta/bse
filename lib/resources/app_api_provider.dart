import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter_bse/model/commodity_model.dart';
import 'package:flutter_bse/model/currency_model.dart';
import 'package:flutter_bse/model/equity_gainer_model.dart';
import 'package:flutter_bse/model/home_indices.dart';
import 'package:flutter_bse/model/sensex_model.dart';
import 'package:flutter_bse/model/sensex_scode_model.dart';
import 'package:flutter_bse/utils/const.dart';

class AppApiProvider {
  var dio;
  HomeIndices homeIndices;

  Future<List<dynamic>> getHomeHeaderData(String jsonEncode) async {
    dio = Dio();
    dio.options.headers["accept"] = 'application/json';
    Response response = await dio
        .get(Constants.BASE_URL + Constants.HEADER_URL + "?json=" + jsonEncode);
    if (response != null && response.data != null) {
      return response.data;
    }
    return null;
  }

  Future<List<HomeIndices>> getHomeIndicesData() async {
    dio = Dio();
    List<HomeIndices> getIndices = [];
    Map<String, dynamic> map = new Map();
    map["flag"] = "";
    map["ln"] = "en";
    map["pg"] = "1";
    map["cnt"] = "15";
    map["fields"] = "1,2,3,4,5,6";
    map["hmpg"] = "1";
    dio.options.headers["accept"] = 'application/json';
    Response response = await dio.get(Constants.BASE_URL +
        Constants.INDICES_URL +
        "?json=" +
        jsonEncode(map));
    if (response != null && response.data != null) {
      for (var item in response.data) {
        HomeIndices homeIndices = HomeIndices.fromJson(item);
        getIndices.add(homeIndices);
      }
      return getIndices;
    }
    return null;
  }

  Future<List<CommodityModel>> getCommodityData() async {
    dio = Dio();
    List<CommodityModel> getCommodity = [];
    dio.options.headers["accept"] = 'application/json';
    Response response = await dio
        .get(Constants.BASE_URL + Constants.COMMODITY_URL, queryParameters: {
      'Flag': '8',
      'Records': '1000',
      'scripcode': '0',
      'pg': '1',
      'cnt': '6',
      'field':'CHANGE,CHANGEPERC,INSTRUMENTNAME,LASTTRADERATE,SCRIPCODE,EXPIRYDATE,DT_TM',
      'ln': 'en',
    });
    if (response != null && response.data != null) {
      for (var item in response.data) {
        CommodityModel homeIndices = CommodityModel.fromJson(item);
        getCommodity.add(homeIndices);
      }
      print(response.data);
      return getCommodity;
    }
    return null;
  }

  Future<List<CurrencyModel>> getCurrencyData() async {
    dio = Dio();
    List<CurrencyModel> getCurrency = [];
    dio.options.headers["accept"] = 'application/json';
    Response response = await dio
        .get(Constants.BASE_URL + Constants.CURRENCY_URL, queryParameters: {
      'Flag': '9',
      'Records': '1000',
      'scripcode': '0',
      'pg': '2',
      'cnt': '5',
      'field':
          'CHANGE,CHANGEPERC,INSTRUMENTNAME,LASTTRADERA TE,SCRIPCODE,EXPIRYDATE,DT_TM',
      'ln': 'en',
    });
    if (response != null && response.data != null) {
      for (var item in response.data) {
        CurrencyModel homeIndices = CurrencyModel.fromJson(item);
        getCurrency.add(homeIndices);
      }
      return getCurrency;
    }
    return null;
  }

  Future<List<SensexModel>> getSensexData() async {
    dio = Dio();
    List<SensexModel> getSensex = [];
    Map<String, dynamic> map = new Map();
    map["icode"] = "16";
    map["order"] = "";
    map["pg"] = "1";
    map["cnt"] = "6";
    map["fields"] = "1";
    dio.options.headers["accept"] = 'application/json';
    Response response = await dio.get(Constants.BASE_URL + Constants.SENSEX_URL + "?json=" + jsonEncode(map));
    if (response != null && response.data != null) {
      for (var item in response.data) {
        SensexModel homeIndices = SensexModel.fromJson(item);
        getSensex.add(homeIndices);
      }
      return getSensex;
    }
    return null;
  }

  Future<ScodeModel> getScodeData(String scode) async {
    dio = Dio();
    Map<String, dynamic> map = new Map();
    map["scode"] = scode;
    map["ln"] = "en";
    map["fields"] = "1,2,3,4,11,12";
    dio.options.headers["accept"] = 'application/json';
    Response response = await dio.get(Constants.BASE_URL + Constants.SENSEX_URL_2 + "?json=" + jsonEncode(map));
    if (response != null && response.data != null) {
      ScodeModel homeScode = ScodeModel.fromJson(response.data[0]);
      return homeScode;
    }
    return null;
  }

  Future<GainerModel> getGainerData(String encryptKey) async {
    dio = Dio();
    dio.options.headers["Sdt"] = encryptKey;
    Response response = await dio.get(Constants.BASE_URL + Constants.EQUITY_GAINER_URL);
    if (response != null && response.data != null) {
      print(response.data);
      if (response.data["status"] == "success") {
        GainerModel gainerModel = GainerModel.fromJson(response.data[0]);
        return gainerModel;
      } else {
        return null;
      }
    }
    return null;
  }
}
