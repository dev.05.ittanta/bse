import 'package:flutter_bse/model/commodity_model.dart';
import 'package:flutter_bse/model/currency_model.dart';
import 'package:flutter_bse/model/home_indices.dart';
import 'package:flutter_bse/model/sensex_model.dart';
import 'package:flutter_bse/model/sensex_scode_model.dart';
import 'package:flutter_bse/resources/repository.dart';

class HomeBloc {
  final _repository = Repository();

  Future<List<dynamic>> homeHeaderData(String jsonEncode) async {
    List<dynamic> homeHeader =
        await _repository.getHomeHeaderDetails(jsonEncode);
    return homeHeader;
  }

  Future<List<HomeIndices>> homeIndicesData() async {
    List<HomeIndices> homeIndices = await _repository.getHomeIndicesDetails();
    return homeIndices;
  }

  Future<List<CommodityModel>> homeCommodityData() async {
    List<CommodityModel> homeCommodity =
        await _repository.getCommodityDetails();
    return homeCommodity;
  }

  Future<List<CurrencyModel>> homeCurrencyData() async {
    List<CurrencyModel> homeCurrency = await _repository.getCurrencyDetails();
    return homeCurrency;
  }

  Future<List<SensexModel>> homeSensexData() async {
    List<SensexModel> homeSensex = await _repository.getSensex();
    return homeSensex;
  }

  Future<ScodeModel> homeScodeData(String scode) async {
    ScodeModel homeScode = await _repository.getScode(scode);
    return homeScode;
  }
}

final homeBloc = HomeBloc();
