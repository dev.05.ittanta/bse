
import 'dart:core';

import 'package:flutter_bse/model/equity_gainer_model.dart';
import 'package:flutter_bse/resources/repository.dart';
import 'package:rxdart/rxdart.dart';

class GainerDetailBlock {
  final _repository = Repository();
  final _gainerDetailBlocFetcher = PublishSubject<GainerModel>();
  Stream<GainerModel> get gainerData => _gainerDetailBlocFetcher.stream;

  Future <GainerModel> fetchGainerDetailsData(String encryptKey) async {
   GainerModel gainerModel = await _repository.getGainerDetails(encryptKey);
   _gainerDetailBlocFetcher.sink.add(gainerModel);

  }

  dispose() {
    _gainerDetailBlocFetcher.close();
  }
}

final gainerDetailBloc = GainerDetailBlock();
