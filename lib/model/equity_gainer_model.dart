class GainerModel {
  String status;
  List<GainerData> data;

  GainerModel({this.status, this.data});

  GainerModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      data = new List<GainerData>();
      json['data'].forEach((v) {
        data.add(new GainerData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class GainerData {
  String scripName;
  String lTradeRt;
  String changeVal;
  String changePer;
  String scripCd;

  GainerData(
      {this.scripName,
        this.lTradeRt,
        this.changeVal,
        this.changePer,
        this.scripCd});

  GainerData.fromJson(Map<String, dynamic> json) {
    scripName = json['ScripName'];
    lTradeRt = json['LTradeRt'];
    changeVal = json['ChangeVal'];
    changePer = json['ChangePer'];
    scripCd = json['ScripCd'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ScripName'] = this.scripName;
    data['LTradeRt'] = this.lTradeRt;
    data['ChangeVal'] = this.changeVal;
    data['ChangePer'] = this.changePer;
    data['ScripCd'] = this.scripCd;
    return data;
  }
}
