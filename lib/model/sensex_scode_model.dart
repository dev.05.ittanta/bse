class ScodeModel {
  String scode;
  String sname;
  String ltp;
  String prevClose;
  String chgval;
  String chgper;

  ScodeModel(
      {this.scode,
        this.sname,
        this.ltp,
        this.prevClose,
        this.chgval,
        this.chgper});

  ScodeModel.fromJson(Map<String, dynamic> json) {
    scode = json['scode'];
    sname = json['sname'];
    ltp = json['ltp'];
    prevClose = json['PrevClose'];
    chgval = json['chgval'];
    chgper = json['chgper'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['scode'] = this.scode;
    data['sname'] = this.sname;
    data['ltp'] = this.ltp;
    data['PrevClose'] = this.prevClose;
    data['chgval'] = this.chgval;
    data['chgper'] = this.chgper;
    return data;
  }
}
