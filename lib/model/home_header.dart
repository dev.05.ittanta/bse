class HomeHeaderModel {
  String indxnm;
  String ltp;
  String chg;
  String perchg;
  String f;
  String dttm;
  String istream;
  String msg;
  String prevClose;
  String iOpen;
  String high;
  String low;
  String source;

  HomeHeaderModel(
      {this.indxnm,
        this.ltp,
        this.chg,
        this.perchg,
        this.f,
        this.dttm,
        this.istream,
        this.msg,
        this.prevClose,
        this.iOpen,
        this.high,
        this.low,
        this.source});

  HomeHeaderModel.fromJson(Map<String, dynamic> json) {
    indxnm = json['indxnm'];
    ltp = json['ltp'];
    chg = json['chg'];
    perchg = json['perchg'];
    f = json['F'];
    dttm = json['dttm'];
    istream = json['istream'];
    msg = json['msg'];
    prevClose = json['Prev_Close'];
    iOpen = json['I_open'];
    high = json['High'];
    low = json['Low'];
    source = json['source'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['indxnm'] = this.indxnm;
    data['ltp'] = this.ltp;
    data['chg'] = this.chg;
    data['perchg'] = this.perchg;
    data['F'] = this.f;
    data['dttm'] = this.dttm;
    data['istream'] = this.istream;
    data['msg'] = this.msg;
    data['Prev_Close'] = this.prevClose;
    data['I_open'] = this.iOpen;
    data['High'] = this.high;
    data['Low'] = this.low;
    data['source'] = this.source;
    return data;
  }
}
