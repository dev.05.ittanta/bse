class HomeIndices {
  String indxnm;
  String code;
  String ltp;
  String chg;
  String perchg;
  String pgord;

  HomeIndices(
      {this.indxnm, this.code, this.ltp, this.chg, this.perchg, this.pgord});

  HomeIndices.fromJson(Map<String, dynamic> json) {
    indxnm = json['indxnm'];
    code = json['code'];
    ltp = json['ltp'];
    chg = json['chg'];
    perchg = json['perchg'];
    pgord = json['pgord'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['indxnm'] = this.indxnm;
    data['code'] = this.code;
    data['ltp'] = this.ltp;
    data['chg'] = this.chg;
    data['perchg'] = this.perchg;
    data['pgord'] = this.pgord;
    return data;
  }
}
