class CurrencyModel {
  String eXPIRYDATE;
  String dTTM;
  int pageCount;
  String cHANGEPERC;
  String cHANGE;
  String lASTTRADERATE;
  String sCRIPCODE;
  String iNSTRUMENTNAME;

  CurrencyModel(
      {this.eXPIRYDATE,
        this.dTTM,
        this.pageCount,
        this.cHANGEPERC,
        this.cHANGE,
        this.lASTTRADERATE,
        this.sCRIPCODE,
        this.iNSTRUMENTNAME});

  CurrencyModel.fromJson(Map<String, dynamic> json) {
    eXPIRYDATE = json['EXPIRYDATE'];
    dTTM = json['DT_TM'];
    pageCount = json['PageCount'];
    cHANGEPERC = json['CHANGEPERC'];
    cHANGE = json['CHANGE'];
    lASTTRADERATE = json['LASTTRADERATE'];
    sCRIPCODE = json['SCRIPCODE'];
    iNSTRUMENTNAME = json['INSTRUMENTNAME'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['EXPIRYDATE'] = this.eXPIRYDATE;
    data['DT_TM'] = this.dTTM;
    data['PageCount'] = this.pageCount;
    data['CHANGEPERC'] = this.cHANGEPERC;
    data['CHANGE'] = this.cHANGE;
    data['LASTTRADERATE'] = this.lASTTRADERATE;
    data['SCRIPCODE'] = this.sCRIPCODE;
    data['INSTRUMENTNAME'] = this.iNSTRUMENTNAME;
    return data;
  }
}
