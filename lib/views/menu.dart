import 'package:flutter/material.dart';
import 'package:flutter_bse/main.dart';
import 'package:flutter_bse/utils/color_constants.dart';
import 'package:flutter_bse/utils/const.dart';
import 'package:flutter_bse/views/base.dart';
import 'package:flutter_bse/views/equity.dart';
import 'package:flutter_bse/views/main_screen.dart';
import 'package:flutter_bse/views/settings.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class Menu extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MenuState();
  }
}

class MenuState extends Base<Menu> {
  ListView List_Criteria;
  bool isExpand = true;
  bool isExpandNewEquity = false;
  String Gainer;
   List<bool> expandList = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: themeChangeProvider.darkTheme
          ? ColorConstants.darkBG
          : ColorConstants.lightBG,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListView(
            shrinkWrap: true,
            physics: ScrollPhysics(),
            children: [
              Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: IconButton(
                          icon: Icon(Icons.close,
                              color: themeChangeProvider.darkTheme
                                  ? ColorConstants.white
                                  : ColorConstants.darkBG),
                          onPressed: () {
                            Navigator.of(context).pop(context);
                            push(MainScreen());
                          }),
                    ),
                  ),
                  IconButton(
                      icon: Icon(Icons.person_pin,
                          color: themeChangeProvider.darkTheme
                              ? ColorConstants.white
                              : ColorConstants.darkBG),
                      onPressed: () {}),
                  IconButton(
                      icon: Icon(Icons.settings,
                          color: themeChangeProvider.darkTheme
                              ? ColorConstants.white
                              : ColorConstants.darkBG),
                      onPressed: () {
                        push(Setting());
                      })
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Home",
                      style: TextStyle(
                          fontSize: 14.sp,
                          fontWeight: FontWeight.bold,
                          color: themeChangeProvider.darkTheme
                              ? ColorConstants.white
                              : ColorConstants.darkBG),
                    ),
                    SizedBox(height: 10.h),
                    Divider(
                        height: 1,
                        color: themeChangeProvider.darkTheme
                            ? ColorConstants.white
                            : ColorConstants.darkBG),
                    SizedBox(height: 10.h),
                    equityExpandUi("Equity"),
                    Text(
                      "Indices",
                      style: TextStyle(
                          fontSize: 14.sp,
                          fontWeight: FontWeight.bold,
                          color: themeChangeProvider.darkTheme
                              ? ColorConstants.white
                              : ColorConstants.darkBG),
                    ),
                    SizedBox(height: 10.h),
                    Divider(
                        height: 1,
                        color: themeChangeProvider.darkTheme
                            ? ColorConstants.white
                            : ColorConstants.darkBG),
                    SizedBox(height: 10.h),
                    expandui("Sensex"),
                    expandui("SME"),
                    expandui("Derivatives"),
                    expandui("Currency"),
                    expandui("Commodity"),
                    expandui("IRD"),
                    expandui("ETF"),
                    expandui("Debt"),
                    expandui("Corporates"),
                    Text(
                      "Market Statistics",
                      style: TextStyle(
                          fontSize: 14.sp,
                          fontWeight: FontWeight.bold,
                          color: themeChangeProvider.darkTheme
                              ? ColorConstants.white
                              : ColorConstants.darkBG),
                    ),
                    SizedBox(height: 10.h),
                    Divider(
                        height: 1,
                        color: themeChangeProvider.darkTheme
                            ? ColorConstants.white
                            : ColorConstants.darkBG),
                    SizedBox(height: 10.h),
                    Text(
                      "Market Turnover",
                      style: TextStyle(
                          fontSize: 14.sp,
                          fontWeight: FontWeight.bold,
                          color: themeChangeProvider.darkTheme
                              ? ColorConstants.white
                              : ColorConstants.darkBG),
                    ),
                    SizedBox(height: 10.h),
                    Divider(
                        height: 1,
                        color: themeChangeProvider.darkTheme
                            ? ColorConstants.white
                            : ColorConstants.darkBG),
                    SizedBox(height: 10.h),
                    Text(
                      "IPO/OFS",
                      style: TextStyle(
                          fontSize: 14.sp,
                          fontWeight: FontWeight.bold,
                          color: themeChangeProvider.darkTheme
                              ? ColorConstants.white
                              : ColorConstants.darkBG),
                    ),
                    SizedBox(height: 10.h),
                    Divider(
                        height: 1,
                        color: themeChangeProvider.darkTheme
                            ? ColorConstants.white
                            : ColorConstants.darkBG),
                    SizedBox(height: 10.h),
                    Text(
                      "Listings",
                      style: TextStyle(
                          fontSize: 14.sp,
                          fontWeight: FontWeight.bold,
                          color: themeChangeProvider.darkTheme
                              ? ColorConstants.white
                              : ColorConstants.darkBG),
                    ),
                    SizedBox(height: 10.h),
                    Divider(
                        height: 1,
                        color: themeChangeProvider.darkTheme
                            ? ColorConstants.white
                            : ColorConstants.darkBG),
                    SizedBox(height: 10.h),
                    Text(
                      "Notices",
                      style: TextStyle(
                          fontSize: 14.sp,
                          fontWeight: FontWeight.bold,
                          color: themeChangeProvider.darkTheme
                              ? ColorConstants.white
                              : ColorConstants.darkBG),
                    ),
                    SizedBox(height: 10.h),
                    Divider(
                        height: 1,
                        color: themeChangeProvider.darkTheme
                            ? ColorConstants.white
                            : ColorConstants.darkBG),
                    SizedBox(height: 10.h),
                    Text(
                      "Watchlist",
                      style: TextStyle(
                          fontSize: 14.sp,
                          fontWeight: FontWeight.bold,
                          color: themeChangeProvider.darkTheme
                              ? ColorConstants.white
                              : ColorConstants.darkBG),
                    ),
                    SizedBox(height: 10.h),
                    Divider(
                        height: 1,
                        color: themeChangeProvider.darkTheme
                            ? ColorConstants.white
                            : ColorConstants.darkBG),
                    SizedBox(height: 10.h),
                    Text(
                      "Portfolio",
                      style: TextStyle(
                          fontSize: 14.sp,
                          fontWeight: FontWeight.bold,
                          color: themeChangeProvider.darkTheme
                              ? ColorConstants.white
                              : ColorConstants.darkBG),
                    ),
                    SizedBox(height: 10.h),
                    Divider(
                        height: 1,
                        color: themeChangeProvider.darkTheme
                            ? ColorConstants.white
                            : ColorConstants.darkBG),
                    SizedBox(height: 10.h),
                    Text(
                      "Listings",
                      style: TextStyle(
                          fontSize: 14.sp,
                          fontWeight: FontWeight.bold,
                          color: themeChangeProvider.darkTheme
                              ? ColorConstants.white
                              : ColorConstants.darkBG),
                    ),
                    SizedBox(height: 10.h),
                    Divider(
                        height: 1,
                        color: themeChangeProvider.darkTheme
                            ? ColorConstants.white
                            : ColorConstants.darkBG),
                    SizedBox(height: 10.h),
                    Text(
                      "Get quote Equity",
                      style: TextStyle(
                          fontSize: 14.sp,
                          fontWeight: FontWeight.bold,
                          color: themeChangeProvider.darkTheme
                              ? ColorConstants.white
                              : ColorConstants.darkBG),
                    ),
                    SizedBox(height: 10.h),
                    Divider(
                        height: 1,
                        color: themeChangeProvider.darkTheme
                            ? ColorConstants.white
                            : ColorConstants.darkBG),
                    SizedBox(height: 10.h),
                    Text(
                      "Get quote Equity New",
                      style: TextStyle(
                          fontSize: 14.sp,
                          fontWeight: FontWeight.bold,
                          color: themeChangeProvider.darkTheme
                              ? ColorConstants.white
                              : ColorConstants.darkBG),
                    ),
                    SizedBox(height: 10.h),
                    Divider(
                        height: 1,
                        color: themeChangeProvider.darkTheme
                            ? ColorConstants.white
                            : ColorConstants.darkBG),
                    SizedBox(height: 10.h),
                    Text(
                      "Get quote Equity New with Box",
                      style: TextStyle(
                          fontSize: 14.sp,
                          fontWeight: FontWeight.bold,
                          color: themeChangeProvider.darkTheme
                              ? ColorConstants.white
                              : ColorConstants.darkBG),
                    ),
                    SizedBox(height: 10.h),
                    Divider(
                        height: 1,
                        color: themeChangeProvider.darkTheme
                            ? ColorConstants.white
                            : ColorConstants.darkBG),
                    SizedBox(height: 10.h),
                    Text(
                      "Edit Watchlist and Portfolio",
                      style: TextStyle(
                          fontSize: 14.sp,
                          fontWeight: FontWeight.bold,
                          color: themeChangeProvider.darkTheme
                              ? ColorConstants.white
                              : ColorConstants.darkBG),
                    ),
                    SizedBox(height: 10.h),
                    Divider(
                        height: 1,
                        color: themeChangeProvider.darkTheme
                            ? ColorConstants.white
                            : ColorConstants.darkBG),
                    SizedBox(height: 10.h),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  equityExpandUi(String string) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: InkWell(
                child: Text(
                  string,
                  style: TextStyle(
                      fontSize: 14.sp,
                      fontWeight: FontWeight.bold,
                      color: themeChangeProvider.darkTheme
                          ? ColorConstants.white
                          : ColorConstants.darkBG),
                ),
                onTap: () {
                  push(EquityScreen(screenName: "Gainer"));
                },
              ),
            ),
            InkWell(
              onTap: () {
                setState(() {
                  isExpand = !isExpand;
                });
              },
              child: Padding(
                padding: const EdgeInsets.fromLTRB(8.0,0,8,0),
                child: Icon(Icons.keyboard_arrow_down,
                    size: 30,
                    color: themeChangeProvider.darkTheme
                        ? ColorConstants.white
                        : ColorConstants.darkBG),
              ),
            ),
          ],
        ),
        SizedBox(height: 5.h),
        isExpand
            ? Align(
                alignment: Alignment.centerLeft,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    InkWell(
                      child: Text(
                        "GAINERS",
                        style: TextStyle(
                            fontSize: 14.sp,
                            fontWeight: FontWeight.bold,
                            color: themeChangeProvider.darkTheme
                                ? ColorConstants.white
                                : ColorConstants.darkBG),
                      ),
                      onTap: () {
                        push(EquityScreen(screenName: Constants.GAINER));
                      },
                    ),
                    SizedBox(
                      height: 5.h,
                    ),
                    InkWell(
                      child: Text(
                        "LOSERS",
                        style: TextStyle(
                            fontSize: 14.sp,
                            fontWeight: FontWeight.bold,
                            color: themeChangeProvider.darkTheme
                                ? ColorConstants.white
                                : ColorConstants.darkBG),
                      ),
                      onTap: () {
                        push(EquityScreen(screenName: Constants.LOSERS));
                      },
                    ),
                    SizedBox(
                      height: 5.h,
                    ),
                    InkWell(
                      child: Text(
                        "TOP TURNOVER",
                        style: TextStyle(
                            fontSize: 14.sp,
                            fontWeight: FontWeight.bold,
                            color: themeChangeProvider.darkTheme
                                ? ColorConstants.white
                                : ColorConstants.darkBG),
                      ),
                      onTap: () {
                        push(EquityScreen(screenName: Constants.TOP_TURNOVER));
                      },
                    ),
                    SizedBox(
                      height: 5.h,
                    ),
                    InkWell(
                      child: Text(
                        "52 WK HIGH",
                        style: TextStyle(
                            fontSize: 14.sp,
                            fontWeight: FontWeight.bold,
                            color: themeChangeProvider.darkTheme
                                ? ColorConstants.white
                                : ColorConstants.darkBG),
                      ),
                      onTap: () {
                        push(EquityScreen(
                            screenName: Constants.FIFTYTWO_WK_HIGH));
                      },
                    ),
                    SizedBox(
                      height: 5.h,
                    ),
                    InkWell(
                      child: Text(
                        "52 WK LOW",
                        style: TextStyle(
                            fontSize: 14.sp,
                            fontWeight: FontWeight.bold,
                            color: themeChangeProvider.darkTheme
                                ? ColorConstants.white
                                : ColorConstants.darkBG),
                      ),
                      onTap: () {
                        push(EquityScreen(
                            screenName: Constants.FIFTYTWO_WK_LOW));
                      },
                    ),
                    SizedBox(
                      height: 5.h,
                    ),
                  ],
                ),
              )
            : SizedBox(),
        Divider(
            height: 1,
            color: themeChangeProvider.darkTheme
                ? ColorConstants.white
                : ColorConstants.darkBG),
        SizedBox(height: 10.h),
      ],
    );
  }

  expandui(String string) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              flex: 2,
              child: Text(
                string,
                style: TextStyle(
                    fontSize: 14.sp,
                    fontWeight: FontWeight.bold,
                    color: themeChangeProvider.darkTheme
                        ? ColorConstants.white
                        : ColorConstants.darkBG),
              ),
            ),
            Expanded(
              flex: 2,
              child: InkWell(
                onTap: () {},
                child: Icon(
                    isExpand
                        ? Icons.arrow_forward_ios
                        : Icons.keyboard_arrow_down,
                    size: 20,
                    color: themeChangeProvider.darkTheme
                        ? ColorConstants.white
                        : ColorConstants.darkBG),
              ),
            ),
          ],
        ),
        SizedBox(height: 10.h),
        Divider(
            height: 1,
            color: themeChangeProvider.darkTheme
                ? ColorConstants.white
                : ColorConstants.darkBG),
        SizedBox(height: 10.h),
      ],
    );
  }

}
