
import 'package:flutter/material.dart';
import 'package:flutter_bse/views/settings.dart';

class Profile extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ProfileState();
  }
}
class ProfileState extends State<Profile>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0.0,
        actions: [
          IconButton(icon:Icon(Icons.settings,color: Colors.white,), onPressed: (){
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (BuildContext context){
                  return Setting();
                },
              ),
            );
          }),
        ],
      ),
      body: Text("Profile"),
    );
  }

}