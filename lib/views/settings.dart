import 'package:flutter/material.dart';
import 'package:flutter_bse/main.dart';
import 'package:flutter_bse/theme/dark_theme_provider.dart';
import 'package:flutter_bse/utils/color_constants.dart';
import 'package:flutter_bse/utils/const.dart';
import 'package:flutter_bse/views/main_screen.dart';
import 'package:provider/provider.dart';

import 'base.dart';

class Setting extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SettingState();
  }
}

class SettingState extends Base<Setting> {
  final darkController = TextEditingController();
  final lightController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final themeChange = Provider.of<DarkThemeProvider>(context);
    return Scaffold(
      body: ListView(
        shrinkWrap: true,
        physics: ScrollPhysics(),
        padding: EdgeInsets.fromLTRB(20,50,20,20),
        children: [
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Expanded(
                  flex: 1,
                  child: Row(
                    children: [
                      Icon(Icons.arrow_forward_ios,
                          size: 20,
                          color: themeChangeProvider.darkTheme?ColorConstants.white:ColorConstants.darkBG),
                      SizedBox(width: 20),
                      Text(
                        "Theme",
                        style: TextStyle(
                          fontSize: 17,
                        ),
                      ),
                    ],
                  )),
              Expanded(
                flex: 2,
                child: InkWell(
                  child: Row(
                    children: [
                      Text(
                        "Theme",
                        style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      SizedBox(width: 10),
                      Icon(Icons.arrow_drop_down_sharp,
                          size: 20,
                          color: themeChangeProvider.darkTheme?Colors.white:Colors.black),
                    ],
                  ),
                  onTap: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          backgroundColor: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.0))),
                          content: Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              InkWell(
                                child: Row(
                                  children: [
                                    Text("Dark",
                                        style: TextStyle(color: Colors.black)),
                                  ],
                                ),
                                onTap: (){
                                  setState(() {
                                    themeChange.darkTheme = true;
                                  });
                                  Navigator.pop(context);
                                  push(MainScreen());
                                },
                              ),
                              SizedBox(height: 20),
                              InkWell(
                                child: Text("Light",
                                    style: TextStyle(color: Colors.black)),
                                onTap: (){
                                  setState(() {
                                    themeChange.darkTheme = false;
                                  });
                                  Navigator.pop(context);
                                  push(MainScreen());

                                },
                              ),
                            ],
                          ),
                        );
                      },
                    );
                  },
                ),
              ),
            ],
          ),

        ],
      ),
    );
  }
}
