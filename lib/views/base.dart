import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bse/utils/const.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class Base<T extends StatefulWidget> extends State<T> {
  GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  final TextEditingController _searchControl = new TextEditingController();
  SharedPreferences prefs;
  BuildContext dialogContext;

  @override
  void initState() {
    super.initState();
    initPreference();
  }

  Future<void> initPreference() async {
    prefs = await SharedPreferences.getInstance();
  }

  // This method is about push to new widget and replace current widget
  pushReplacement(StatefulWidget screenName) {
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (context) => screenName));
  }

  // This method is about push to new widget but don't replace current widget
  push(StatefulWidget screenName) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => screenName));
  }

  // This method is about push to new widget and remove all previous widget
  pushAndRemoveUntil(StatefulWidget screenName) {
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (BuildContext context) => screenName),
        (_) => false);
  }
  Route _createRoute(StatefulWidget screenName) {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => screenName,
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        return child;
      },
    );
  }
  Future<bool> initUserData() async {
    prefs = await SharedPreferences.getInstance();
    return true;
  }

  void showMessage(String message) {
    setState(() {});
  }

  showLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        dialogContext = context;
        return Center(child: CircularProgressIndicator());
      },
    );
  }

  hideLoading() {
    if (dialogContext != null) {
      Navigator.pop(dialogContext);
    }
  }

  // // Show loading with optional message params
  // showLoading({String msg}) {
  //   if (msg != null) {
  //     EasyLoading.show(status: msg);
  //   } else {
  //     EasyLoading.show();
  //   }
  // }
  //
  // hideLoadingSuccess(String msg) async {
  //   EasyLoading.showSuccess(msg, duration: Duration(seconds: 2));
  //   await Future.delayed(Duration(seconds: 3));
  //   EasyLoading.dismiss();
  // }
  //
  // hideLoadingError(String msg) async {
  //   EasyLoading.showError(msg, duration: Duration(seconds: 2));
  //   await Future.delayed(Duration(seconds: 3));
  //   EasyLoading.dismiss();
  // }
  //
  // hideLoading() {
  //   EasyLoading.dismiss();
  // }

  /*
   * Show Snackbar with Global scaffold key
   * scaffoldKey is defined globally as snackbar do not find context of Scaffold widget
   * hideLoading is hide the loader when snackbar message is showing to UI
   */
  showSnackBar(String msg) {
    ScaffoldMessenger.of(context)
        .showSnackBar(new SnackBar(content: Text(msg)));
  }

  //
  // showMessage(String title, String message) {
  //   hideLoading();
  //   if (Platform.isAndroid) {
  //     showDialog(
  //       context: context,
  //       barrierDismissible: false,
  //       builder: (BuildContext ctxt) {
  //         return AlertDialog(
  //           title: Text(
  //             title,
  //             style: TextStyle(
  //               fontFamily: "Montserrat",
  //               fontSize: 22,
  //               fontWeight: FontWeight.bold,
  //               color: Colors.black,
  //             ),
  //           ),
  //           content: Text(
  //             message,
  //             style: TextStyle(
  //               fontFamily: "Montserrat",
  //               fontSize: 18,
  //               color: Colors.black,
  //             ),
  //           ),
  //           actions: <Widget>[
  //             FlatButton(
  //               onPressed: () {
  //                 Navigator.pop(context);
  //               },
  //               child: Text(
  //                 "Ok",
  //                 style: TextStyle(
  //                   fontFamily: "Montserrat",
  //                   fontWeight: FontWeight.bold,
  //                 ),
  //               ),
  //             ),
  //           ],
  //         );
  //       },
  //     );
  //   }
  //   if (Platform.isIOS) {
  //     showCupertinoDialog(
  //       context: context,
  //       builder: (BuildContext ctxt) {
  //         return CupertinoAlertDialog(
  //           title: Text(
  //             title,
  //             style: TextStyle(
  //               fontFamily: "Montserrat",
  //               fontSize: 22,
  //               fontWeight: FontWeight.bold,
  //               color: Colors.black,
  //             ),
  //           ),
  //           content: Text(
  //             message,
  //             style: TextStyle(
  //               fontFamily: "Montserrat",
  //               fontSize: 18,
  //               color: Colors.black,
  //             ),
  //           ),
  //           actions: <Widget>[
  //             FlatButton(
  //               onPressed: () {
  //                 Navigator.pop(context);
  //               },
  //               child: Text(
  //                 "Ok",
  //                 style: TextStyle(
  //                   fontFamily: "Montserrat",
  //                   fontWeight: FontWeight.bold,
  //                 ),
  //               ),
  //             ),
  //           ],
  //         );
  //       },
  //     );
  //   }
  // }

// Check Internet Connection Async method with Snackbar message.
  Future<bool> isConnected() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      }
    } on SocketException catch (_) {
      showSnackBar(Constants.INTERNET_CHECK);
      return false;
    }
    showSnackBar(Constants.INTERNET_CHECK);
    return false;
  }

}
