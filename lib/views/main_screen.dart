import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bse/main.dart';
import 'package:flutter_bse/utils/color_constants.dart';
import 'package:flutter_bse/views/home.dart';
import 'package:flutter_bse/views/menu.dart';
import 'package:flutter_bse/views/portfolio.dart';
import 'package:flutter_bse/views/search.dart';
import 'package:flutter_bse/views/watchlist.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  static List<Widget> _widgetOptions = <Widget>[
    WatchList(),
    Portfolio(),
    Home(),
    Search(),
    Menu(),
  ];

  int _selectedIndex = 2;
  PageController _pageController;

  Color _getBgColor(int index) => _selectedIndex == index
      ? ColorConstants.select_tab_color
      : Colors.transparent;

  Color _getItemColor(int index) => _selectedIndex == index
      ? Colors.white
      : Theme.of(context).textTheme.caption.color;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      _pageController.animateToPage(index,
          duration: Duration(milliseconds: 200), curve: Curves.easeInToLinear);
    });
  }

  Widget _buildIcon(String text, int index, String asset1, String asset2,
      String asset3, String asset4) {
    return Container(
        width: double.infinity,
        height: kBottomNavigationBarHeight,
        color: _getBgColor(index),
        child: InkWell(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _selectedIndex == index
                  ? themeChangeProvider.darkTheme
                      ? Image.asset(asset1, width: 23, height: 23)
                      : Image.asset(asset2, width: 23, height: 23)
                  : themeChangeProvider.darkTheme
                      ? Image.asset(asset3, width: 23, height: 23)
                      : Image.asset(asset4, width: 23, height: 23),
              Text(text,
                  style: TextStyle(
                      fontSize: 11,
                      color: _getItemColor(index),
                      fontWeight: FontWeight.bold)),
            ],
          ),
          onTap: () {
            _onItemTapped(index);
          },
        ));
  }

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: 2);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: PageView(
            controller: _pageController,
            physics: NeverScrollableScrollPhysics(),
            onPageChanged: (index) {
              setState(() {
                _selectedIndex = index;
              });
            },
            children: _widgetOptions),
        bottomNavigationBar: BottomNavigationBar(
            elevation: 10,
            currentIndex: _selectedIndex,
            onTap: (int index) {
              setState(() {
                _selectedIndex = index;
              });
            },
            type: BottomNavigationBarType.fixed,
            backgroundColor: themeChangeProvider.darkTheme
                ? ColorConstants.bg_color
                : ColorConstants.white,
            selectedFontSize: 0,
            items: [
              BottomNavigationBarItem(
                icon: _buildIcon(
                    "Watchlist",
                    0,
                    'assets/images/dark/watchlist_active.png',
                    'assets/images/light/watchlist_active.png',
                    'assets/images/dark/watchlist_deactive.png',
                    'assets/images/light/watchlist_deactive.png'),
                title: SizedBox.shrink(),
              ),
              BottomNavigationBarItem(
                icon: _buildIcon(
                    "Portfolio",
                    1,
                    'assets/images/dark/portfolio_active.png',
                    'assets/images/light/portfolio_active.png',
                    'assets/images/dark/portfolio_deactive.png',
                    'assets/images/light/portfolio_deactive.png'),
                title: SizedBox.shrink(),
              ),
              BottomNavigationBarItem(
                //backgroundColor: ColorConstants.select_tab_color,
                icon: _buildIcon(
                    "Home",
                    2,
                    'assets/images/bse_logo.png',
                    'assets/images/bse_logo.png',
                    'assets/images/bse_logo.png',
                    'assets/images/bse_logo.png'),
                title: SizedBox.shrink(),
              ),
              BottomNavigationBarItem(
                icon: _buildIcon(
                    "Search",
                    3,
                    'assets/images/dark/search_active.png',
                    'assets/images/light/search_active.png',
                    'assets/images/dark/search_deactive.png',
                    'assets/images/light/search_deactive.png'),
                title: SizedBox.shrink(),
              ),
              new BottomNavigationBarItem(
                icon: _buildIcon(
                    "Menu",
                    4,
                    'assets/images/dark/menu_active.png',
                    'assets/images/light/menu_active.png',
                    'assets/images/dark/menu_deactive.png',
                    'assets/images/light/menu_deactive.png'),
                title: SizedBox.shrink(),
              )
            ]));
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }
}
