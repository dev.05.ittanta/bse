import 'dart:convert';
import 'dart:typed_data';

import 'package:encrypt/encrypt.dart' as encrypt;
import 'package:flutter/material.dart';
import 'package:flutter_bse/blocs/equity_block.dart';
import 'package:flutter_bse/main.dart';
import 'package:flutter_bse/model/equity_gainer_model.dart';
import 'package:flutter_bse/utils/color_constants.dart';
import 'package:flutter_bse/utils/const.dart';
import 'package:flutter_bse/views/base.dart';
import 'package:flutter_bse/views/profile.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';

class EquityScreen extends StatefulWidget {
  var screenName;

  EquityScreen({this.screenName});

  @override
  State<StatefulWidget> createState() {
    return EquityScreenState();
  }
}

class EquityScreenState extends Base<EquityScreen>
    with SingleTickerProviderStateMixin {
  String screenName, encryptedString;
  TabController _tabController;
  int currentTab = 0;
  GainerModel _gainerModel;

  // GainerData gainerData;
  List<GainerData> gainerData = [];

  String dateTimeString;
  var encrypter;
  var iv;

  @override
  void initState() {
    super.initState();
    screenName = widget.screenName;
    _tabController = new TabController(vsync: this, length: 5);

    if (screenName == Constants.GAINER) {
      currentTab = 0;
      _tabController.index = 0;
    } else if (screenName == Constants.LOSERS) {
      currentTab = 1;
      _tabController.index = 1;
    } else if (screenName == Constants.TOP_TURNOVER) {
      currentTab = 2;
      _tabController.index = 2;
    } else if (screenName == Constants.FIFTYTWO_WK_HIGH) {
      currentTab = 3;
      _tabController.index = 3;
    } else if (screenName == Constants.FIFTYTWO_WK_LOW) {
      currentTab = 4;
      _tabController.index = 4;
    }
    gainerData.add(new GainerData(
        scripName: "ANUHPHR",
        lTradeRt: "135",
        changeVal: "-15.70",
        changePer: "-10.27",
        scripCd: "506260"));
    gainerData.add(new GainerData(
        scripName: "ANUHPHR",
        lTradeRt: "135",
        changeVal: "-15.70",
        changePer: "-10.27",
        scripCd: "506260"));
    gainerData.add(new GainerData(
        scripName: "ANUHPHR",
        lTradeRt: "135",
        changeVal: "-15.70",
        changePer: "-10.27",
        scripCd: "506260"));
    gainerData.add(new GainerData(
        scripName: "ANUHPHR",
        lTradeRt: "135",
        changeVal: "-15.70",
        changePer: "-10.27",
        scripCd: "506260"));
    gainerData.add(new GainerData(
        scripName: "ANUHPHR",
        lTradeRt: "135",
        changeVal: "-15.70",
        changePer: "-10.27",
        scripCd: "506260"));
    gainerData.add(new GainerData(
        scripName: "ANUHPHR",
        lTradeRt: "135",
        changeVal: "-15.70",
        changePer: "-10.27",
        scripCd: "506260"));
    gainerData.add(new GainerData(
        scripName: "ANUHPHR",
        lTradeRt: "135",
        changeVal: "-15.70",
        changePer: "-10.27",
        scripCd: "506260"));
    gainerData.add(new GainerData(
        scripName: "ANUHPHR",
        lTradeRt: "135",
        changeVal: "-15.70",
        changePer: "-10.27",
        scripCd: "506260"));
    gainerData.add(new GainerData(
        scripName: "ANUHPHR",
        lTradeRt: "135",
        changeVal: "-15.70",
        changePer: "-10.27",
        scripCd: "506260"));
    gainerData.add(new GainerData(
        scripName: "ANUHPHR",
        lTradeRt: "135",
        changeVal: "-15.70",
        changePer: "-10.27",
        scripCd: "506260"));
    gainerData.add(new GainerData(
        scripName: "ANUHPHR",
        lTradeRt: "135",
        changeVal: "-15.70",
        changePer: "-10.27",
        scripCd: "506260"));
    gainerData.add(new GainerData(
        scripName: "ANUHPHR",
        lTradeRt: "135",
        changeVal: "-15.70",
        changePer: "-10.27",
        scripCd: "506260"));
  }

  @override
  Widget build(BuildContext context) {
    gainerDetailBloc.fetchGainerDetailsData(getDateEncrypted());
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: themeChangeProvider.darkTheme
          ? ColorConstants.darkBG
          : ColorConstants.lightBG,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: themeChangeProvider.darkTheme
            ? ColorConstants.darkBG
            : ColorConstants.lightBG,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              size: 30,
              color: themeChangeProvider.darkTheme
                  ? ColorConstants.white
                  : ColorConstants.darkBG,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
        actions: <Widget>[
          Builder(builder: (context) {
            return Row(
              children: [
                InkWell(
                  child: Image.asset(
                    "assets/images/google_assit.png",
                    width: 30,
                    height: 30,
                  ),
                  onTap: () {},
                ),
                SizedBox(
                  width: 20,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 15),
                  child: InkWell(
                    child: themeChangeProvider.darkTheme
                        ? Image.asset(
                            "assets/images/dark/menu_icon_dark.png",
                            width: 25,
                            height: 25,
                          )
                        : Image.asset(
                            "assets/images/light/menu_icon_light.png",
                            width: 25,
                            height: 25,
                          ),
                    onTap: () {
                      push(Profile());
                    },
                  ),
                ),
              ],
            );
          })
        ],
        bottom: PreferredSize(
            preferredSize: Size.fromHeight(70),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Container(
                width: MediaQuery.of(context).size.width,
                child: TabBar(
                    controller: _tabController,
                    onTap: (index) {
                      setState(() {
                        _tabController.index = index;
                      });
                    },
                    isScrollable: true,
                    indicatorSize: TabBarIndicatorSize.label,
                    indicator: BoxDecoration(),
                    labelPadding: EdgeInsets.symmetric(horizontal: 2.0),
                    tabs: [
                      Tab(
                        child: Card(
                          color: _tabController.index == 0
                              ? ColorConstants.select_tab_color
                              : themeChangeProvider.darkTheme
                                  ? ColorConstants.dark_gray
                                  : Colors.white,
                          child: Align(
                            alignment: Alignment.center,
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(25, 5, 25, 5),
                              child: Text("Gainer",
                                  style: TextStyle(
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.bold,
                                      color: _tabController.index == 0
                                          ? ColorConstants.white
                                          : themeChangeProvider.darkTheme
                                              ? ColorConstants.white
                                              : ColorConstants.black)),
                            ),
                          ),
                        ),
                      ),
                      Tab(
                        child: Card(
                          color: _tabController.index == 1
                              ? ColorConstants.select_tab_color
                              : themeChangeProvider.darkTheme
                                  ? ColorConstants.dark_gray
                                  : Colors.white,
                          child: Align(
                            alignment: Alignment.center,
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(25, 5, 25, 5),
                              child: Text("Losers",
                                  style: TextStyle(
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.bold,
                                      color: _tabController.index == 1
                                          ? ColorConstants.white
                                          : themeChangeProvider.darkTheme
                                              ? ColorConstants.white
                                              : ColorConstants.black)),
                            ),
                          ),
                        ),
                      ),
                      Tab(
                        child: Card(
                          color: _tabController.index == 2
                              ? ColorConstants.select_tab_color
                              : themeChangeProvider.darkTheme
                                  ? ColorConstants.dark_gray
                                  : Colors.white,
                          child: Align(
                            alignment: Alignment.center,
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(25, 5, 25, 5),
                              child: Text("Top Turnover",
                                  style: TextStyle(
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.bold,
                                      color: _tabController.index == 2
                                          ? ColorConstants.white
                                          : themeChangeProvider.darkTheme
                                              ? ColorConstants.white
                                              : ColorConstants.black)),
                            ),
                          ),
                        ),
                      ),
                      Tab(
                        child: Card(
                          color: _tabController.index == 3
                              ? ColorConstants.select_tab_color
                              : themeChangeProvider.darkTheme
                                  ? ColorConstants.dark_gray
                                  : Colors.white,
                          child: Align(
                            alignment: Alignment.center,
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(25, 5, 25, 5),
                              child: Text("52 WK High",
                                  style: TextStyle(
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.bold,
                                      color: _tabController.index == 3
                                          ? ColorConstants.white
                                          : themeChangeProvider.darkTheme
                                              ? ColorConstants.white
                                              : ColorConstants.black)),
                            ),
                          ),
                        ),
                      ),
                      Tab(
                        child: Card(
                          color: _tabController.index == 4
                              ? ColorConstants.select_tab_color
                              : themeChangeProvider.darkTheme
                                  ? ColorConstants.dark_gray
                                  : Colors.white,
                          child: Align(
                            alignment: Alignment.center,
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(25, 5, 25, 5),
                              child: Text("52 WK Low",
                                  style: TextStyle(
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.bold,
                                      color: _tabController.index == 4
                                          ? ColorConstants.white
                                          : themeChangeProvider.darkTheme
                                              ? ColorConstants.white
                                              : ColorConstants.black)),
                            ),
                          ),
                        ),
                      ),
                    ]),
              ),
            )),
      ),
      body: TabBarView(
        controller: _tabController,
        children: [
          gainerUi(),

          // ListView(
          //   shrinkWrap: true,
          //   physics: ScrollPhysics(),
          //   children: [
          //
          //     // StreamBuilder(
          //     //   stream: gainerDetailBloc.gainerData,
          //     //   builder: (context, AsyncSnapshot<GainerModel> snapshot) {
          //     //     if (snapshot.hasData) {
          //     //       _gainerModel = snapshot.data;
          //     //       return _gainerModel != null ? gainerUi() : SizedBox();
          //     //     } else if (snapshot.hasError) {
          //     //       return Text(snapshot.error.toString());
          //     //     }
          //     //     return Center(child: CircularProgressIndicator());
          //     //   },
          //     // ),
          //   ],
          // ),
          Center(
            child: Container(
              child: Text(
                "Losers",
                style: TextStyle(fontSize: 22, color: Colors.blue),
              ),
            ),
          ),
          Center(
            child: Container(
              child: Text(
                "Top turnover",
                style: TextStyle(fontSize: 22, color: Colors.blue),
              ),
            ),
          ),
          Center(
            child: Container(
              child: Text(
                "52 High",
                style: TextStyle(fontSize: 22, color: Colors.blue),
              ),
            ),
          ),
          Center(
            child: Container(
              child: Text(
                "52 Low",
                style: TextStyle(fontSize: 22, color: Colors.blue),
              ),
            ),
          ),
        ],
      ),
    );
  }

  // Widget gainerUi() {
  //   return ListView.builder(
  //       shrinkWrap: true,
  //       physics: ScrollPhysics(),
  //       itemCount: _gainerModel.data.length,
  //       itemBuilder: (BuildContext context, int index) {
  //         var gainerItem = _gainerModel.data[index];
  //         return Card(
  //           margin: EdgeInsets.all(2),
  //           child: Container(
  //             margin: EdgeInsetsDirectional.only(start: 2, end: 2),
  //             decoration: BoxDecoration(
  //               border: Border(
  //                   bottom: BorderSide(
  //                 color: double.parse(gainerItem.scripName) > 0
  //                     ? Colors.green
  //                     : Colors.red,
  //               )),
  //             ),
  //             child: Column(
  //               mainAxisSize: MainAxisSize.min,
  //               children: <Widget>[
  //                 Expanded(
  //                     child: Padding(
  //                   padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
  //                   child: Row(
  //                     children: [
  //                       Expanded(
  //                         child: Text(
  //                           gainerItem.lTradeRt,
  //                           overflow: TextOverflow.ellipsis,
  //                           style: TextStyle(
  //                             fontSize: 14,
  //                             fontWeight: FontWeight.bold,
  //                           ),
  //                         ),
  //                       ),
  //                       SizedBox(
  //                         width: 40.w,
  //                       ),
  //                     ],
  //                   ),
  //                 )),
  //                 Expanded(
  //                   child: Padding(
  //                     padding: EdgeInsets.fromLTRB(8, 0, 8, 8),
  //                     child: Row(
  //                       children: [
  //                         SizedBox(width: 30.w),
  //                         Text(
  //                           gainerItem.changeVal,
  //                           style: TextStyle(
  //                               color: double.parse(gainerItem.changeVal) > 0
  //                                   ? Colors.green
  //                                   : Colors.red,
  //                               fontSize: 13,
  //                               fontWeight: FontWeight.bold),
  //                         ),
  //                         SizedBox(width: 10.w),
  //                         Text(
  //                           gainerItem.changePer + "%",
  //                           style: TextStyle(
  //                             color: double.parse(gainerItem.changePer) > 0
  //                                 ? Colors.green
  //                                 : Colors.red,
  //                             fontSize: 13,
  //                             fontWeight: FontWeight.bold,
  //                           ),
  //                         ),
  //                       ],
  //                     ),
  //                   ),
  //                 ),
  //               ],
  //             ),
  //           ),
  //         );
  //       });
  // }
  Widget gainerUi() {
    return gainerData != null
        ? ListView.builder(
            shrinkWrap: true,
            physics: ScrollPhysics(),
            itemCount: gainerData.length,
            itemBuilder: (BuildContext context, int index) {
              var gainerItem = gainerData[index];
              return Card(
                margin: EdgeInsets.all(10),
                child: Container(
                  padding: EdgeInsets.all(5),
                  margin: EdgeInsetsDirectional.only(start: 2, end: 2),
                  decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                      color: double.parse(gainerItem.lTradeRt) > 0
                          ? Colors.green
                          : Colors.red,
                    )),
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              gainerItem.scripName,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 40.w,
                          ),
                          Text(
                            gainerItem.lTradeRt,
                            style: TextStyle(
                                color: double.parse(gainerItem.lTradeRt) > 0
                                    ? Colors.green
                                    : Colors.red,
                                fontSize: 13,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(child: SizedBox()),
                          Text(
                            gainerItem.changeVal,
                            style: TextStyle(
                                color: double.parse(gainerItem.changeVal) > 0
                                    ? Colors.green
                                    : Colors.red,
                                fontSize: 13,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(width: 10.w),
                          Text(
                            gainerItem.changePer + "%",
                            style: TextStyle(
                              color: double.parse(gainerItem.changePer) > 0
                                  ? Colors.green
                                  : Colors.red,
                              fontSize: 13,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              );
            })
        : CircularProgressIndicator();
  }

  String getDateEncrypted() {
    List<int> ivInt = utf8.encode(Constants.ENCRYPT_KEY_IV);
    Uint8List ivBytes = Uint8List.fromList(ivInt);
    List<int> keyInt = utf8.encode(Constants.ENCRYPT_KEY_IV);
    Uint8List keyBytes = Uint8List.fromList(keyInt);
    final key = encrypt.Key(keyBytes);
    iv = encrypt.IV(ivBytes);
    encrypter = encrypt.Encrypter(
        encrypt.AES(key, mode: encrypt.AESMode.cbc, padding: "PKCS7"));
    DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
    dateTimeString = dateFormat.format(DateTime.now().toUtc());
    final encrypted = encrypter.encrypt(dateTimeString, iv: iv);
    encryptedString = encrypted.base64;
    print(encryptedString);
    return encryptedString;
  }
}
