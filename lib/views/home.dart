import 'dart:convert';

import 'package:admob_flutter/admob_flutter.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bse/blocs/home_bloc.dart';
import 'package:flutter_bse/google_ads/ads_helper.dart';
import 'package:flutter_bse/main.dart';
import 'package:flutter_bse/model/commodity_model.dart';
import 'package:flutter_bse/model/currency_model.dart';
import 'package:flutter_bse/model/home_header.dart';
import 'package:flutter_bse/model/home_indices.dart';
import 'package:flutter_bse/model/sensex_model.dart';
import 'package:flutter_bse/model/sensex_scode_model.dart';
import 'package:flutter_bse/utils/color_constants.dart';
import 'package:flutter_bse/views/base.dart';
import 'package:flutter_bse/views/profile.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:ink_page_indicator/ink_page_indicator.dart';
import 'package:intl/intl.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomeState();
  }
}

final List<WatchListModel> watchlistItemList = [
  WatchListModel(name: 'AsianPaint', ltp: "2825", rtp: "2980"),
];

class HomeState extends Base<Home> {
  HomeHeaderModel headerData;
  List<HomeIndices> getIndicesList = [];
  List<CommodityModel> getCommodityList = [];
  List<CurrencyModel> getCurrencyList = [];
  List<SensexModel> getSensexList = [];
  List<ScodeModel> sCodeModelList = [];

  int commodityViewPagerCount = 0;
  int currencyViewPagerCount = 0;
  int sensexViewPagerCount = 0;

  final ValueNotifier<double> commodityPage = ValueNotifier(0.0);
  final ValueNotifier<double> currencyPage = ValueNotifier(0.0);
  final ValueNotifier<double> sensexPage = ValueNotifier(0.0);

  PageIndicatorController commodityController,
      currencyController,
      sensexController;

  IndicatorShape shape;
  IndicatorShape activeShape;
  String marketUpdate = "";
  List<String> getHeaderDate;
  String headerDate = "";
  double height;
  double width;
  var format = NumberFormat.currency(locale: 'HI');
  final CarouselController _controller = CarouselController();
  AdmobBannerSize bannerSize;
  AdmobBannerSize nativeBannerSize;
  AdmobInterstitial interstitialAd;
  AdmobBanner admobBanner;

  @override
  void initState() {
    super.initState();
    bannerSize = AdmobBannerSize.BANNER;
    nativeBannerSize = AdmobBannerSize.MEDIUM_RECTANGLE;

    commodityController = PageIndicatorController()
      ..addListener(() {
        commodityPage.value = commodityController.page;
      });
    currencyController = PageIndicatorController()
      ..addListener(() {
        currencyPage.value = currencyController.page;
      });
    sensexController = PageIndicatorController()
      ..addListener(() {
        sensexPage.value = sensexController.page;
      });

    Future.delayed(Duration.zero, () async {
      await getHeaderData();
      await getIndicesData();
      await getCommodityData();
      await getCurrencyData();
      await getSensexData();
    });
  }

  @override
  Widget build(BuildContext context) {
    shape = IndicatorShape.circle(5);
    activeShape = IndicatorShape.circle(5);
    height = MediaQuery
        .of(context)
        .size
        .height;
    width = MediaQuery
        .of(context)
        .size
        .width;
    return Scaffold(
        key: scaffoldKey,
        backgroundColor: themeChangeProvider.darkTheme
            ? ColorConstants.darkBG
            : ColorConstants.lightBG,
        body:
        SafeArea(
          child: Padding(
            padding: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
            child: ListView(
              shrinkWrap: true,
              physics: ScrollPhysics(),
              children: [
                headerData != null ? headerUi() : SizedBox(),
                SizedBox(
                  height: 20.h,
                ),
                AdmobBanner(
                  adUnitId: AdMobHelper.getBannerAdUnitId(),
                  adSize: bannerSize,
                  // listener: (AdmobAdEvent event,
                  //     Map<String, dynamic> args) {
                  //   handleEvent(event, args, 'Banner');
                  // },
                  onBannerCreated: (AdmobBannerController controller) {},
                ),
                SizedBox(
                  height: 20.h,
                ),
                watchlistUi(),
                SizedBox(
                  height: 20.h,
                ),
                indicesUi(),
                SizedBox(
                  height: 30.h,
                ),
                commodityUi(),
                SizedBox(
                  height: 10.h,
                ),
                AdmobBanner(
                  adUnitId: AdMobHelper.getBannerAdUnitId(),
                  adSize: nativeBannerSize,
                  // listener: (AdmobAdEvent event,
                  //     Map<String, dynamic> args) {
                  //   handleEvent(event, args, 'NativeBanner');
                  // },
                  onBannerCreated: (AdmobBannerController controller) {},
                ),
                SizedBox(
                  height: 30.h,
                ),
                sensexUi(),
                SizedBox(
                  height: 10.h,
                ),
                currencyUi(),
              ],
            ),
          ),
        ));
  }

  headerUi() {
    return headerData != null
        ? Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              InkWell(
                child: Image.asset(
                  "assets/images/google_assit.png",
                  width: 30,
                  height: 30,
                ),
                onTap: () {},
              ),
              SizedBox(
                width: 10,
              ),
              InkWell(
                child: themeChangeProvider.darkTheme
                    ? Image.asset(
                  "assets/images/dark/menu_icon_dark.png",
                  width: 25,
                  height: 25,
                )
                    : Image.asset(
                  "assets/images/light/menu_icon_light.png",
                  width: 25,
                  height: 25,
                ),
                onTap: () {
                  push(Profile());
                },
              ),
            ],
          ),
        ),
        Image.asset(
          "assets/images/home_header.png",
          width: 170.w,
          height: 50.h,
          fit: BoxFit.fill,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              headerData.ltp != null ? headerData.ltp : "",
              style: TextStyle(
                  fontSize: 19,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Arial'),
            ),
            SizedBox(width: 10.w),
            Text(
              headerData.chg != null ? headerData.chg : "",
              style: TextStyle(
                  color: double.parse(headerData.chg) > 0
                      ? Colors.green
                      : Colors.red,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Arial'),
            ),
            SizedBox(width: 10),
            Text(
              headerData.perchg != null ? headerData.perchg + "%" : "",
              style: TextStyle(
                  color: double.parse(headerData.chg) > 0
                      ? Colors.green
                      : Colors.red,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Arial'),
            ),
          ],
        ),
        SizedBox(height: 10.h),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              getHeaderDate != null
                  ? getHeaderDate[0] + " | " + getHeaderDate[1]
                  : "",
              style: TextStyle(
                  color: themeChangeProvider.darkTheme
                      ? ColorConstants.white
                      : ColorConstants.grey_color,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Arial'),
            ),
            Text(
              headerData.f != null ? ("  |  " + marketUpdate) : "",
              style: TextStyle(
                  color: themeChangeProvider.darkTheme
                      ? ColorConstants.white
                      : ColorConstants.grey_color,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Arial'),
            ),
          ],
        ),
      ],
    )
        : Center(child: CircularProgressIndicator());
  }

  indicesUi() {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: Text(
                "Indices",
                style: TextStyle(
                    fontSize: 19,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Arial'),
              ),
            ),
            InkWell(
              child: Icon(Icons.more_horiz,
                  color: themeChangeProvider.darkTheme
                      ? Colors.white
                      : Colors.black),
              onTap: () {},
            )
          ],
        ),
        SizedBox(
          height: 10.h,
        ),
        getIndicesList != null
            ? Container(
          width: double.infinity,
          height: 350.h,
          child: GridView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: getIndicesList.length,
            gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: width / 6,
              childAspectRatio: 1 / 5,
              crossAxisSpacing: 5,
              mainAxisSpacing: 8,
            ),
            itemBuilder: (context, index) {
              var item = getIndicesList[index];
              return Card(
                margin: EdgeInsets.all(2),
                child: Container(
                  margin: EdgeInsetsDirectional.only(start: 2, end: 2),
                  decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                          color: double.parse(item.perchg) > 0
                              ? Colors.green
                              : Colors.red,
                        )),
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Expanded(
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Text(
                                    item.indxnm,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'Arial'),
                                  ),
                                ),
                                SizedBox(
                                  width: 40.w,
                                ),
                                Text(
                                  format
                                      .format(double.parse(item.ltp))
                                      .replaceAll("INR", ""),
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'Arial'),
                                ),
                              ],
                            ),
                          )),
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(8, 0, 8, 8),
                          child: Row(
                            children: [
                              Expanded(
                                child: Text(
                                  getHeaderDate[0],
                                  style: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'Arial'),
                                ),
                              ),
                              SizedBox(width: 30.w),
                              Text(
                                item.chg,
                                style: TextStyle(
                                    color: double.parse(item.chg) > 0
                                        ? Colors.green
                                        : Colors.red,
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Arial'),
                              ),
                              SizedBox(width: 10.w),
                              Text(
                                item.perchg + "%",
                                style: TextStyle(
                                    color: double.parse(item.perchg) > 0
                                        ? Colors.green
                                        : Colors.red,
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Arial'),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        )
            : CircularProgressIndicator(),
      ],
    );
  }

  watchlistUi() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Expanded(
              child: Text(
                "Watchlist",
                style: TextStyle(
                    fontSize: 19,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Arial'),
              ),
            ),
            InkWell(
              child: Icon(Icons.more_horiz,
                  color: themeChangeProvider.darkTheme
                      ? Colors.white
                      : Colors.black),
              onTap: () {},
            )
          ],
        ),
        SizedBox(height: 5.h),
        Container(
          child: CarouselSlider(
            items: watchList,
            options: CarouselOptions(
                enlargeCenterPage: true, height: 80.h, viewportFraction: 0.6),
            carouselController: _controller,
          ),
        ),
      ],
    );
  }

  final List<Widget> watchList = watchlistItemList.map((item) {
    return Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        child: Container(
          margin: EdgeInsetsDirectional.only(start: 2, end: 2),
          decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(
                  color: double.parse(item.ltp) > 0 ? Colors.green : Colors.red,
                )),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  item.name,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Arial'),
                ),
                SizedBox(height: 5.h),
                Row(
                  children: [
                    // Text(
                    //   double.parse(commodityItem1.cHANGE) >
                    //       0
                    //       ? "+"
                    //       : "-",
                    //   style: TextStyle(
                    //       color: double.parse(
                    //           commodityItem1
                    //               .cHANGE) >
                    //           0
                    //           ? Colors.green
                    //           : Colors.red,
                    //       fontSize: 13,
                    //       fontWeight: FontWeight.bold,
                    //       fontFamily: 'Arial'),
                    // ),
                    Text(
                      item.ltp,
                      style: TextStyle(
                          color: double.parse(item.ltp) > 0
                              ? Colors.green
                              : Colors.red,
                          fontSize: 13,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Arial'),
                    ),
                    SizedBox(width: 10.w),
                    Text(
                      item.rtp + "%",
                      style: TextStyle(
                          color: double.parse(item.rtp) > 0
                              ? Colors.green
                              : Colors.red,
                          fontSize: 13,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Arial'),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ));
  }).toList();

  commodityUi() {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: Text(
                "Commodity",
                style: TextStyle(
                    fontSize: 19,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Arial'),
              ),
            ),
            InkWell(
              child: Icon(Icons.more_horiz,
                  color: themeChangeProvider.darkTheme
                      ? Colors.white
                      : Colors.black),
              onTap: () {},
            )
          ],
        ),
        SizedBox(height: 5.h),
        InkWell(
          onTap: () =>
              commodityController.animateToPage(
                commodityController.page != commodityViewPagerCount
                    ? commodityViewPagerCount
                    : 0,
                duration: const Duration(milliseconds: 800),
                curve: Curves.ease,
              ),
          child: getCommodityList != null
              ? Container(
            height: 110,
            child: PageView(
              controller: commodityController,
              children: List.generate(commodityViewPagerCount, (index) {
                int itemIndex = index * 2;
                CommodityModel commodityItem1 =
                getCommodityList[itemIndex];
                CommodityModel commodityItem2;
                itemIndex = itemIndex + 1;
                if (itemIndex < getCommodityList.length) {
                  commodityItem2 = getCommodityList[itemIndex];
                }
                return Row(
                  children: [
                    Expanded(
                      child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        margin: EdgeInsets.symmetric(
                            horizontal: 10, vertical: 4),
                        child: Container(
                          margin: EdgeInsetsDirectional.only(
                              start: 2, end: 2),
                          decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                  color:
                                  double.parse(commodityItem1.cHANGE) > 0
                                      ? Colors.green
                                      : Colors.red,
                                )),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  commodityItem1.iNSTRUMENTNAME,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'Arial'),
                                ),
                                SizedBox(height: 5.h),
                                Text(
                                  commodityItem1.eXPIRYDATE,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      color: ColorConstants.grey_color,
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'Arial'),
                                ),
                                SizedBox(height: 5.h),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      0, 0, 8, 0),
                                  child: Row(
                                    children: [
                                      // Text(
                                      //   double.parse(commodityItem1.cHANGE) >
                                      //       0
                                      //       ? "+"
                                      //       : "-",
                                      //   style: TextStyle(
                                      //       color: double.parse(
                                      //           commodityItem1
                                      //               .cHANGE) >
                                      //           0
                                      //           ? Colors.green
                                      //           : Colors.red,
                                      //       fontSize: 13,
                                      //       fontWeight: FontWeight.bold,
                                      //       fontFamily: 'Arial'),
                                      // ),
                                      Text(
                                        commodityItem1.cHANGE,
                                        style: TextStyle(
                                            color: double.parse(
                                                commodityItem1
                                                    .cHANGE) >
                                                0
                                                ? Colors.green
                                                : Colors.red,
                                            fontSize: 13,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'Arial'),
                                      ),
                                      SizedBox(width: 10.w),
                                      Text(
                                        commodityItem1.cHANGEPERC + "%",
                                        style: TextStyle(
                                            color: double.parse(
                                                commodityItem1
                                                    .cHANGEPERC) >
                                                0
                                                ? Colors.green
                                                : Colors.red,
                                            fontSize: 13,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'Arial'),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    commodityItem2 != null
                        ? Expanded(
                      child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.circular(10)),
                        margin: EdgeInsets.symmetric(
                            horizontal: 10, vertical: 4),
                        child: Container(
                          margin: EdgeInsetsDirectional.only(
                              start: 2, end: 2),
                          decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                  color: double.parse(
                                      commodityItem2.cHANGE) >
                                      0
                                      ? Colors.green
                                      : Colors.red,
                                )),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              mainAxisAlignment:
                              MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  commodityItem2.iNSTRUMENTNAME,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'Arial'),
                                ),
                                SizedBox(height: 5.h),
                                Text(
                                  commodityItem2.eXPIRYDATE,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      color:
                                      ColorConstants.grey_color,
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'Arial'),
                                ),
                                SizedBox(height: 5.h),
                                Padding(
                                  padding:
                                  const EdgeInsets.fromLTRB(
                                      0, 0, 8, 0),
                                  child: Row(
                                    children: [
                                      // Text(
                                      //   double.parse(commodityItem2.cHANGE) >
                                      //       0
                                      //       ? "+"
                                      //       : "-",
                                      //   style: TextStyle(
                                      //       color: double.parse(
                                      //           commodityItem2
                                      //               .cHANGE) >
                                      //           0
                                      //           ? Colors.green
                                      //           : Colors.red,
                                      //       fontSize: 13,
                                      //       fontWeight: FontWeight.bold,
                                      //       fontFamily: 'Arial'),
                                      // ),
                                      Text(
                                        commodityItem2.cHANGE,
                                        style: TextStyle(
                                            color: double.parse(
                                                commodityItem2
                                                    .cHANGE) >
                                                0
                                                ? Colors.green
                                                : Colors.red,
                                            fontSize: 13,
                                            fontWeight:
                                            FontWeight.bold,
                                            fontFamily: 'Arial'),
                                      ),
                                      SizedBox(width: 10.w),
                                      Text(
                                        commodityItem2.cHANGEPERC +
                                            "%",
                                        style: TextStyle(
                                            color: double.parse(
                                                commodityItem2
                                                    .cHANGEPERC) >
                                                0
                                                ? Colors.green
                                                : Colors.red,
                                            fontSize: 13,
                                            fontWeight:
                                            FontWeight.bold,
                                            fontFamily: 'Arial'),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    )
                        : Expanded(child: SizedBox()),
                  ],
                );
              }),
            ),
          )
              : Center(child: CircularProgressIndicator()),
        ),
        commobityPageIndicator(InkStyle.normal),
      ],
    );
  }

  currencyUi() {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: Text(
                "Currency",
                style: TextStyle(
                    fontSize: 19,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Arial'),
              ),
            ),
            InkWell(
              child: Icon(Icons.more_horiz,
                  color: themeChangeProvider.darkTheme
                      ? Colors.white
                      : Colors.black),
              onTap: () {},
            )
          ],
        ),
        SizedBox(height: 5.h),
        InkWell(
          onTap: () =>
              currencyController.animateToPage(
                currencyController.page != currencyViewPagerCount
                    ? currencyViewPagerCount
                    : 0,
                duration: const Duration(milliseconds: 800),
                curve: Curves.ease,
              ),
          child: getCurrencyList != null
              ? Container(
            height: 110,
            child: PageView(
              controller: currencyController,
              children: List.generate(currencyViewPagerCount, (index) {
                int currencyItemIndex = index * 2;
                CurrencyModel currencyItem1 =
                getCurrencyList[currencyItemIndex];
                CurrencyModel currencyItem2;
                currencyItemIndex = currencyItemIndex + 1;
                if (currencyItemIndex < getCurrencyList.length) {
                  currencyItem2 = getCurrencyList[currencyItemIndex];
                }
                return Row(
                  children: [
                    Expanded(
                      child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        margin: EdgeInsets.symmetric(
                            horizontal: 10, vertical: 4),
                        child: Container(
                          margin: EdgeInsetsDirectional.only(
                              start: 2, end: 2),
                          decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                  color:
                                  double.parse(currencyItem1.cHANGE) > 0
                                      ? Colors.green
                                      : Colors.red,
                                )),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  currencyItem1.iNSTRUMENTNAME,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'Arial'),
                                ),
                                SizedBox(height: 5.h),
                                Text(
                                  currencyItem1.eXPIRYDATE,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      color: ColorConstants.grey_color,
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'Arial'),
                                ),
                                SizedBox(height: 5.h),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      0, 0, 8, 0),
                                  child: Row(
                                    children: [
                                      Text(
                                        currencyItem1.cHANGE,
                                        style: TextStyle(
                                            color: double.parse(
                                                currencyItem1
                                                    .cHANGE) >
                                                0
                                                ? Colors.green
                                                : Colors.red,
                                            fontSize: 13,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'Arial'),
                                      ),
                                      SizedBox(width: 10.w),
                                      Text(
                                        currencyItem1.cHANGEPERC + "%",
                                        style: TextStyle(
                                            color: double.parse(
                                                currencyItem1
                                                    .cHANGEPERC) >
                                                0
                                                ? Colors.green
                                                : Colors.red,
                                            fontSize: 13,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'Arial'),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    currencyItem2 != null
                        ? Expanded(
                      child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.circular(10)),
                        margin: EdgeInsets.symmetric(
                            horizontal: 10, vertical: 4),
                        child: Container(
                          margin: EdgeInsetsDirectional.only(
                              start: 2, end: 2),
                          decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                  color: double.parse(
                                      currencyItem2.cHANGE) >
                                      0
                                      ? Colors.green
                                      : Colors.red,
                                )),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              mainAxisAlignment:
                              MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  currencyItem2.iNSTRUMENTNAME,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'Arial'),
                                ),
                                SizedBox(height: 5.h),
                                Text(
                                  currencyItem2.eXPIRYDATE,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      color:
                                      ColorConstants.grey_color,
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'Arial'),
                                ),
                                SizedBox(height: 5.h),
                                Padding(
                                  padding:
                                  const EdgeInsets.fromLTRB(
                                      0, 0, 8, 0),
                                  child: Row(
                                    children: [
                                      // Text(
                                      //   double.parse(currencyItem2.cHANGE) >
                                      //           0
                                      //       ? "+"
                                      //       : "-",
                                      //   style: TextStyle(
                                      //       color: double.parse(
                                      //                   currencyItem2
                                      //                       .cHANGE) >
                                      //               0
                                      //           ? Colors.green
                                      //           : Colors.red,
                                      //       fontSize: 13,
                                      //       fontWeight: FontWeight.bold,
                                      //       fontFamily: 'Arial'),
                                      // ),
                                      Text(
                                        currencyItem2.cHANGE,
                                        style: TextStyle(
                                            color: double.parse(
                                                currencyItem2
                                                    .cHANGE) >
                                                0
                                                ? Colors.green
                                                : Colors.red,
                                            fontSize: 13,
                                            fontWeight:
                                            FontWeight.bold,
                                            fontFamily: 'Arial'),
                                      ),
                                      SizedBox(width: 10.w),
                                      // Text(
                                      //   double.parse(currencyItem2
                                      //       .cHANGEPERC) >
                                      //       0
                                      //       ? "+"
                                      //       : "-",
                                      //   style: TextStyle(
                                      //       color: double.parse(
                                      //           currencyItem2
                                      //               .cHANGE) >
                                      //           0
                                      //           ? Colors.green
                                      //           : Colors.red,
                                      //       fontSize: 13,
                                      //       fontWeight: FontWeight.bold,
                                      //       fontFamily: 'Arial'),
                                      // ),
                                      Text(
                                        currencyItem2.cHANGEPERC +
                                            "%",
                                        style: TextStyle(
                                            color: double.parse(
                                                currencyItem2
                                                    .cHANGEPERC) >
                                                0
                                                ? Colors.green
                                                : Colors.red,
                                            fontSize: 13,
                                            fontWeight:
                                            FontWeight.bold,
                                            fontFamily: 'Arial'),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    )
                        : Expanded(child: SizedBox()),
                  ],
                );
              }),
            ),
          )
              : Center(child: CircularProgressIndicator()),
        ),
        currencyPageIndicator(InkStyle.normal),
      ],
    );
  }

  sensexUi() {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: Text(
                "Sensex Heat Map",
                style: TextStyle(
                    fontSize: 19,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Arial'),
              ),
            ),
            InkWell(
              child: Icon(Icons.show_chart,
                  color: themeChangeProvider.darkTheme
                      ? Colors.white
                      : Colors.black),
              onTap: () {},
            ),
            SizedBox(width: 20),
            InkWell(
              child: Icon(Icons.more_horiz,
                  color: themeChangeProvider.darkTheme
                      ? Colors.white
                      : Colors.black),
              onTap: () {},
            )
          ],
        ),
        SizedBox(height: 5.h),
        InkWell(
          onTap: () =>
              sensexController.animateToPage(
                sensexController.page != sensexViewPagerCount
                    ? sensexViewPagerCount
                    : 0,
                duration: const Duration(milliseconds: 800),
                curve: Curves.ease,
              ),
          child: sCodeModelList != null
              ? Container(
            height: 110,
            child: PageView(
              controller: sensexController,
              children: List.generate(sensexViewPagerCount, (index) {
                int scodeItemIndex = index * 2;
                ScodeModel scodeItem1 = sCodeModelList[scodeItemIndex];
                ScodeModel scodeItem2;
                scodeItemIndex = scodeItemIndex + 1;
                if (scodeItemIndex < sCodeModelList.length) {
                  scodeItem2 = sCodeModelList[scodeItemIndex];
                }
                return Row(
                  children: [
                    Expanded(
                      child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        margin: EdgeInsets.symmetric(
                            horizontal: 10, vertical: 4),
                        child: Container(
                          margin: EdgeInsetsDirectional.only(
                              start: 2, end: 2),
                          decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                  color: double.parse(scodeItem1.chgval) > 0
                                      ? Colors.green
                                      : Colors.red,
                                )),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  scodeItem1.sname,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'Arial'),
                                ),
                                SizedBox(height: 5.h),
                                Text(
                                  headerData.ltp,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'Arial'),
                                ),
                                SizedBox(height: 5.h),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      0, 0, 8, 0),
                                  child: Row(
                                    children: [
                                      // Text(
                                      //   double.parse(scodeItem1.chgval) >
                                      //       0
                                      //       ? "+"
                                      //       : "-",
                                      //   style: TextStyle(
                                      //       color: double.parse(
                                      //           scodeItem1.chgval) >
                                      //           0
                                      //           ? Colors.green
                                      //           : Colors.red,
                                      //       fontSize: 13,
                                      //       fontWeight: FontWeight.bold,
                                      //       fontFamily: 'Arial'),
                                      // ),
                                      Text(
                                        scodeItem1.chgval,
                                        style: TextStyle(
                                            color: double.parse(scodeItem1
                                                .chgval) >
                                                0
                                                ? Colors.green
                                                : Colors.red,
                                            fontSize: 13,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'Arial'),
                                      ),
                                      SizedBox(width: 10.w),
                                      Text(
                                        scodeItem1.chgper + "%",
                                        style: TextStyle(
                                            color: double.parse(scodeItem1
                                                .chgper) >
                                                0
                                                ? Colors.green
                                                : Colors.red,
                                            fontSize: 13,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'Arial'),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    scodeItem2 != null
                        ? Expanded(
                      child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.circular(10)),
                        margin: EdgeInsets.symmetric(
                            horizontal: 10, vertical: 4),
                        child: Container(
                          margin: EdgeInsetsDirectional.only(
                              start: 2, end: 2),
                          decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                  color:
                                  double.parse(scodeItem2.chgval) >
                                      0
                                      ? Colors.green
                                      : Colors.red,
                                )),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              mainAxisAlignment:
                              MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  scodeItem2.sname,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'Arial'),
                                ),
                                SizedBox(height: 5.h),
                                Text(
                                  headerData.ltp,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'Arial'),
                                ),
                                SizedBox(height: 5.h),
                                Padding(
                                  padding:
                                  const EdgeInsets.fromLTRB(
                                      0, 0, 8, 0),
                                  child: Row(
                                    children: [
                                      // Text(
                                      //   double.parse(scodeItem2.chgval) >
                                      //       0
                                      //       ? "+"
                                      //       : "-",
                                      //   style: TextStyle(
                                      //       color: double.parse(
                                      //           scodeItem2.chgval) >
                                      //           0
                                      //           ? Colors.green
                                      //           : Colors.red,
                                      //       fontSize: 13,
                                      //       fontWeight: FontWeight.bold,
                                      //       fontFamily: 'Arial'),
                                      // ),
                                      Text(
                                        scodeItem2.chgval,
                                        style: TextStyle(
                                            color: double.parse(
                                                scodeItem2
                                                    .chgval) >
                                                0
                                                ? Colors.green
                                                : Colors.red,
                                            fontSize: 13,
                                            fontWeight:
                                            FontWeight.bold,
                                            fontFamily: 'Arial'),
                                      ),
                                      SizedBox(width: 10.w),
                                      Text(
                                        scodeItem2.chgper + "%",
                                        style: TextStyle(
                                            color: double.parse(
                                                scodeItem2
                                                    .chgper) >
                                                0
                                                ? Colors.green
                                                : Colors.red,
                                            fontSize: 13,
                                            fontWeight:
                                            FontWeight.bold,
                                            fontFamily: 'Arial'),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    )
                        : Expanded(child: SizedBox()),
                  ],
                );
              }),
            ),
          )
              : Center(child: CircularProgressIndicator()),
        ),
        sensexPageIndicator(InkStyle.normal),
      ],
    );
  }

  @override
  void dispose() {
    commodityController.dispose();
    currencyController.dispose();
    sensexController.dispose();

    super.dispose();
  }

  Widget commobityPageIndicator(InkStyle style) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        InkPageIndicator(
          gap: 10,
          padding: 5,
          shape: shape,
          page: commodityPage,
          pageCount: commodityViewPagerCount,
          activeShape: activeShape,
          inactiveColor: themeChangeProvider.darkTheme
              ? ColorConstants.dark_dot_inactive_color
              : ColorConstants.light_grey_color,
          activeColor: themeChangeProvider.darkTheme
              ? ColorConstants.white
              : ColorConstants.select_tab_color,
          style: style,
        ),
      ],
    );
  }

  Widget currencyPageIndicator(InkStyle style) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        InkPageIndicator(
          gap: 10,
          padding: 5,
          shape: shape,
          page: currencyPage,
          pageCount: currencyViewPagerCount,
          activeShape: activeShape,
          inactiveColor: themeChangeProvider.darkTheme
              ? ColorConstants.dark_dot_inactive_color
              : ColorConstants.light_grey_color,
          activeColor: themeChangeProvider.darkTheme
              ? ColorConstants.white
              : ColorConstants.select_tab_color,
          style: style,
        ),
      ],
    );
  }

  Widget sensexPageIndicator(InkStyle style) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        InkPageIndicator(
          gap: 10,
          padding: 5,
          shape: shape,
          page: sensexPage,
          pageCount: sensexViewPagerCount,
          activeShape: activeShape,
          inactiveColor: themeChangeProvider.darkTheme
              ? ColorConstants.dark_dot_inactive_color
              : ColorConstants.light_grey_color,
          activeColor: themeChangeProvider.darkTheme
              ? ColorConstants.white
              : ColorConstants.select_tab_color,
          style: style,
        ),
      ],
    );
  }

  Future<void> getHeaderData() async {
    // showLoading();
    if(isConnected==true){
      Map<String, dynamic> map = new Map();
      map["name"] = "AppSensex";
      map["fields"] = "2,3,4,5,6,7";
      List<dynamic> res = await homeBloc.homeHeaderData(jsonEncode(map));
      if (res != null) {
        setState(() {
          headerData = HomeHeaderModel.fromJson(res[0]);
          getHeaderDate = headerData.dttm.split("|");
          print(getHeaderDate[0] + " | " + getHeaderDate[1]);
          if (headerData.f != null && headerData.f == "0") {
            marketUpdate = "Open";
          } else if (headerData.f != null && headerData.f == "1") {
            marketUpdate = "Pre-Open";
          } else if (headerData.f != null && headerData.f == "2") {
            marketUpdate = "Close";
          } else if (headerData.f != null && headerData.f == "3") {
            marketUpdate = "Open";
          }
        });
      } else {
        showSnackBar("Data is not found");
      }
    }

  }

  Future<List<HomeIndices>> getIndicesData() async {
    if(isConnected==true) {
      getIndicesList = await homeBloc.homeIndicesData();
      setState(() {});
    }

  }

  Future<List<CommodityModel>> getCommodityData() async {
    if(isConnected==true) {
      getCommodityList = await homeBloc.homeCommodityData();
      setState(() {
        if (getCommodityList != null && getCommodityList.length > 0) {
          commodityViewPagerCount = (getCommodityList.length / 2).round();
          print("viewPagerCount >>>>>> " + commodityViewPagerCount.toString());
        }
      });
    }

  }

  Future<List<CurrencyModel>> getCurrencyData() async {
    if(isConnected==true) {
      getCurrencyList = await homeBloc.homeCurrencyData();
      setState(() {
        if (getCurrencyList != null && getCurrencyList.length > 0) {
          currencyViewPagerCount = (getCurrencyList.length / 2).round();
        }
      });
    }

  }

  Future<List<CurrencyModel>> getSensexData() async {
    if(isConnected==true) {
      getSensexList = await homeBloc.homeSensexData();
      if (getSensexList != null && getSensexList.length > 0) {
        String getscode = getSensexList[0].scode;
        List<String> getScodeList = getscode.split("|");
        sCodeModelList.clear();
        for (int i = 0; i < getScodeList.length; i++) {
          await getSCodeData(getScodeList, i);
        }
      }
    }

  }

  Future<void> getSCodeData(List<String> getScodeList, int position) async {
    if(isConnected==true) {
      ScodeModel sCodeData = await homeBloc.homeScodeData(getScodeList[position]);
      if (sCodeData != null) {
        sCodeModelList.add(sCodeData);
      }
      if (position == (getScodeList.length - 1)) {
        //hideLoading();
        setState(() {
          sensexViewPagerCount = (sCodeModelList.length / 2).round();
        });
      }
    }

  }

  void handleEvent(AdmobAdEvent event, Map<String, dynamic> args,
      String adType) {
    switch (event) {
      case AdmobAdEvent.loaded:
        showSnackBar('New Admob $adType Ad loaded!');
        break;
      case AdmobAdEvent.opened:
        showSnackBar('Admob $adType Ad opened!');
        break;
      case AdmobAdEvent.closed:
        showSnackBar('Admob $adType Ad closed!');
        break;
      case AdmobAdEvent.failedToLoad:
        showSnackBar('Admob $adType failed to load. :(');
        break;
      case AdmobAdEvent.rewarded:
        showDialog(
          context: scaffoldKey.currentContext,
          builder: (BuildContext context) {
            return WillPopScope(
              child: AlertDialog(
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text('Reward callback fired. Thanks Andrew!'),
                    Text('Type: ${args['type']}'),
                    Text('Amount: ${args['amount']}'),
                  ],
                ),
              ),
              onWillPop: () async {
                scaffoldKey.currentState.hideCurrentSnackBar();
                return true;
              },
            );
          },
        );
        break;
      default:
    }
  }

}

class WatchListModel {
  final String name;
  final String ltp;
  final String rtp;

  WatchListModel({
    this.name,
    this.ltp,
    this.rtp,
  });
}
