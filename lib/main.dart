import 'package:admob_flutter/admob_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bse/theme/dark_theme_provider.dart';
import 'package:flutter_bse/theme/dark_theme_styles.dart';
import 'package:flutter_bse/utils/const.dart';
import 'package:flutter_bse/views/base.dart';
import 'package:flutter_bse/views/bottom_item.dart';
import 'package:flutter_bse/views/main_screen.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  Admob.initialize();
  runApp(MyApp());
}

DarkThemeProvider themeChangeProvider = new DarkThemeProvider();

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }

}

class _MyAppState extends Base<MyApp> {
  @override
  void initState() {
    super.initState();
    getCurrentAppTheme();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) {
        return themeChangeProvider;
      },
      child: Consumer<DarkThemeProvider>(
        builder: (BuildContext context, value, Widget child) {
          return ScreenUtilInit(
            designSize: Size(360, 690),
            builder: () {
             return MaterialApp(
                  debugShowCheckedModeBanner: false,
                  title: Constants.APP_NAME,
                  theme: Styles.themeData(
                      themeChangeProvider.darkTheme, context,),
                  builder: (BuildContext context, Widget child) {
                    return GestureDetector(
                      onTap: () {
                        FocusScopeNode currentFocus = FocusScope.of(context);
                        if (!currentFocus.hasPrimaryFocus) {
                          currentFocus.unfocus();
                        }
                      },
                      child: child,
                    );
                  },
                  home: MainScreen()
              );
            },
          );
        },
      ),
    );
  }

  void getCurrentAppTheme() async {
    themeChangeProvider.darkTheme =
    await themeChangeProvider.darkThemePreference.getTheme();
  }
}

